FROM yongoh/rails:6
ENV LANG C.UTF-8
ARG APP_NAME=route_link
ARG APP_HOME=/product

WORKDIR /tmp

ADD Gemfile Gemfile
ADD Gemfile.lock Gemfile.lock
ADD $APP_NAME.gemspec $APP_NAME.gemspec
ADD lib/$APP_NAME/version.rb lib/$APP_NAME/version.rb

RUN bundle install

RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
ADD . $APP_HOME
