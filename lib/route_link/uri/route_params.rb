module RouteLink
  module Uri
    class RouteParams < RouteLink::RouteParams

      class << self

        def to_h(arg)
          case arg
          when Hash then
            arg.map do |key, value|
              case key.to_sym
              when :query then [:query, query_to_hash(value)]
              when :path then [:path_component, value]
              else [key.to_sym, value]
              end
            end.to_h
          when String then
            to_h(URI.parse(arg))
          when URI::Generic then
            to_h(arg.component.map{|method| [method, arg.public_send(method)] })
          else
            to_h(super)
          end
        end

        private

        # @param [Object] query 加工前のURIクエリ
        # @return [Hash] ハッシュ化したURIクエリ
        def query_to_hash(query)
          case query
          when String then Rack::Utils.parse_nested_query(query).deep_symbolize_keys
          when nil then {}
          else query
          end
        end
      end

      param :path_component

      # @param [ActionView::Base] helpers ビューコンテキスト。URIハッシュを返す`#default_url_options`を持つことを期待。
      # @return [URI::Generic] パラメータから生成したURIオブジェクト
      def uri(helpers = nil)
        hash = helpers.respond_to?(:default_url_options) ? helpers.default_url_options.merge(uri_components.compact) : uri_components
        URI.parse(URI::Generic.build2(hash).to_s)
      end

      # @return [Hash] URIオブジェクト生成用のハッシュ
      def uri_components
        to_h.map do |key, value|
          case key
          when :query then
            v = value.to_param
            v = nil if v == ""
            [:query, v]
          when :path_component then [:path, value]
          else [key, value]
          end
        end.to_h
      end

      # @param [ActionView::Base] helpers ビューコンテキスト
      # @return [String] パス
      def path(helpers)
        uri = self.uri(helpers)
        if uri.query.nil?
          uri.path
        else
          [uri.path, uri.query].join("?")
        end
      end

      # @param [ActionView::Base] helpers ビューコンテキスト
      # @return [String] URL
      def url(helpers)
        uri(helpers).to_s
      end

      # @return [String] パス名
      def human_path_name
        RouteLink.human_path_name(uri.path)
      end
    end
  end
end
