module RouteLink
  class Params

    class << self

      # @param [Boolean] bool "_"付きパラメータキーを含めるかどうか
      # @return [Array<Symbol>] 定義されたパラメータキーの配列
      def keys(bool = false)
        @keys ||= []
        keys = @keys
        keys += @keys.map{|key| "_#{key}".to_sym } if bool
        keys += superclass.keys(bool) if superclass.respond_to?(:keys)
        keys
      end

      # @param [Hash,Object] arg ハッシュもしくはこのクラスの全てのパラメータメソッドを持つオブジェクト
      # @return [RouteLink::Params] インスタンス
      def build(arg)
        klass = class_get(arg)
        klass = self if klass > self
        if keys.all?{|key| arg.respond_to?(key) }
          klass.new(default: arg)
        else
          hash = klass.to_h(arg)
          default = hash.fetch(:default, nil)
          default = build(default) if default.is_a?(Hash)
          klass.new(default: default).merge!(hash.except(:default))
        end
      end

      # @param [Object] arg パラメータを表すオブジェクト
      # @return [Class<RouteParams>] 引数の種類に応じたルーティングパラメータクラス
      def class_get(arg)
        self
      end

      # @param [Object] arg パラメータを表すオブジェクト
      # @return [Hash] パラメータ群ハッシュ
      def to_h(arg)
        arg.to_h
      end

      private

      # パラメータメソッド群を定義
      # @param [Array<Symbol>] keys パラメータキーの配列
      def param(*keys, **options)
        keys.each do |key|

          # 加工しない値をセット
          define_method("#{key}=") do |value|
            @params[key] = value
          end

          # 加工する値をセット
          define_method("_#{key}=") do |_value|
            @_params[key] = _value
          end

          # @return [Object] 実体化・マージ済みの値
          define_method(key) do |&block|
            merged_param(key, options, &block)
          end

          @keys ||= []
          @keys << key.to_sym
        end
      end
    end

    # @!attribute [r] default
    #   @return [RouteLink::Params,nil] デフォルト値
    attr_reader :default

    def initialize(default: nil)
      @default = default
      @params = Hash.new(nil)
      @_params = Hash.new(nil)
    end

    # @see ::keys
    def keys
      self.class.keys
    end

    # @param [Symbol] key パラメータキー
    # @yield [key, _value] 仮パラメータ値を加工する処理
    # @return [Object] パラメータ値もしくは処理済みの仮パラメータ値
    def materialized_param(key)
      raise KeyError, "key is undefined: #{key.inspect}" unless keys.include?(key)
      if !@params[key].nil?
        @params[key]
      elsif block_given?
        yield(key, @_params[key])
      else
        @_params[key]
      end
    end

    # @param [Symbol] key パラメータキー
    # @param [Proc,nil] merge_proc 自身の値とデフォルト値をマージする手続き。`nil`を渡した場合はマージしない。
    # @param [Symbol,nil] merge_method 自身の値とデフォルト値をマージする際に使用するマージメソッド名
    # @return [Object] マージ済みの値
    def merged_param(key, merge_proc: nil, merge_method: nil, &block)
      merge_proc ||= proc{|v, d| v.respond_to?(merge_method) && d.respond_to?(merge_method) ? d.public_send(merge_method, v) : v } if merge_method
      value = materialized_param(key, &block)
      if value.nil?
        default.try(key, &block)
      elsif merge_proc && default_value = default.try(key, &block)
        merge_proc.call(value, default_value)
      else
        value
      end
    end

    # パラメータを一度にセット
    # @param [Hash,RouteLink::Params] other パラメータハッシュもしくはパラメータオブジェクト
    def merge!(other)
      self.class.to_h(other).each do |key, value|
        public_send("#{key}=", value)
      end
      self
    end

    # パラメータをマージした新しいパラメータオブジェクトを作成
    # @param [Hash,RouteLink::Params] other パラメータハッシュもしくはパラメータオブジェクト
    # @return [RouteLink::Params] 自身をデフォルト値として持つパラメータオブジェクト
    def merge(other)
      klass = self.class.class_get(other)
      klass = self.class if klass > self.class
      other = other.hash_for_build if other.respond_to?(:hash_for_build, true)
      if other.is_a?(Hash) && other.has_key?(:default)
        x = other
        x = x[:default] while x.has_key?(:default)
        x[:default] = self
        klass.build(other)
      else
        klass.new(default: self).merge!(other)
      end
    end

    # @return [Hash] パラメータハッシュ
    def to_h
      default_hash = self.default_hash
      keys.map do |key|
        _key = "_#{key}".to_sym
        if !@params[key].nil?
          [key, public_send(key)]
        elsif !@_params[key].nil?
          [_key, public_send(key)]
        elsif !default_hash[key].nil?
          [key, default_hash[key]]
        elsif !default_hash[_key].nil?
          [_key, default_hash[_key]]
        else
          [key, nil]
        end
      end.to_h
    end

    # @return [RouteLink::Params] 全てのパラメータ値を実体化したパラメータオブジェクト
    # @see #merged_param
    def materialize(*args, &block)
      self.class.new.merge!(_materialize(*args, &block))
    end

    def _materialize(&materialization_proc)
      keys.map do |key|
        [key, merged_param(key, &materialization_proc)]
      end.to_h
    end

    protected

    def hash_for_build
      keys.map do |key|
        _key = "_#{key}".to_sym
        if !@params[key].nil?
          [key, @params[key]]
        elsif !@_params[key].nil?
          [_key, @_params[key]]
        else
          [key, nil]
        end
      end.to_h.merge(
        default: default ? default.hash_for_build : default_hash,
      )
    end

    def default_hash
      default.try(:to_h) || keys.map{|key| [key, default.try(key)] }.to_h
    end
  end
end
