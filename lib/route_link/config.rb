module RouteLink
  class Config
    include ParamsSet
    param route_params: RouteParams, link_params: LinkParams
  end
end
