module RouteLink
  class LinkBuilder
    include ParamsSet

    param route_params: RouteParams, link_params: LinkParams

    class << self

      # @param [ActionView::Base] helpers ビューコンテキスト
      # @param [String,Symbol] format フォーマット文字列もしくはフォーマットキー
      # @param [RouteLink::RouteParams,Object] route_params ルーティングパラメータ
      # @param [RouteLink::LinkParams,Object] link_params リンクパラメータ
      # @yield [] 内容ブロック
      # @return [RouteLink::LinkBuilder] このクラスのインスタンス
      def link_build(helpers, format = nil, route_params = {}, link_params = {}, &block)
        format, route_params, link_params = nil, format, route_params unless format.is_a?(Symbol) || format.is_a?(String)

        route_params = RouteParams.query_build(route_params) if route_params.is_a?(Hash)

        link_params =
          if link_params.is_a?(Hash)
            LinkParams.link_build(helpers, format, link_params, &block)
          else
            LinkParams.build(link_params).merge(format: format)
          end

        new(helpers).merge!(route_params, link_params)
      end

      # @param [ActionView::Base] helpers ビューコンテキスト
      # @return [RouteLink::LinkBuilder] デフォルトの設定を持つリンクビルダー
      def build(helpers)
        config = Rails.configuration.x.route_link
        new(helpers).merge!(config)
      end
    end

    delegate :html_attributes, to: :link_params

    # @param [ActionView::Base] helpers ビューコンテキスト
    def initialize(helpers)
      @helpers = helpers
    end

    # @param [String,Symbol] format フォーマット文字列もしくはフォーマットキー
    # @param [Hash] i18n_params 訳文パラメータ
    # @yield [] 内容ブロック
    # @return [String] 翻訳したルーティング名
    def name(format = nil, **i18n_params, &block)
      merged_link_params.link_merge(@helpers, format, i18n_params: i18n_params, &block).name
    end

    %i(path url).each do |method_type|

      # パス/URLメソッド
      define_method(method_type) do |**query|
        route_params.merge(query: query).public_send(method_type, @helpers)
      end
    end

    # @return [ActiveSupport::SafeBuffer] リンク要素
    # @see ::link_build
    def link(*args, &block)
      if args.any? || block_given?
        link_merge(*args, &block).link
      else
        @helpers.link_to(name, path, html_attributes)
      end
    end

    # @return [RouteLink::LinkBuilder] 自身と渡したパラメータをマージしたリンクビルダー
    # @see ::link_build
    def link_merge(*args, &block)
      merge(self.class.link_build(@helpers, *args, &block))
    end

    private

    def merged_link_params
      LinkParams.build(i18n_params: route_params.i18n_params.merge(i18n_params)).merge(link_params)
    end

    def i18n_params
      @i18n_params ||= %i(path url).map do |method_name|
        [method_name, proc{ public_send(method_name) }]
      end.to_h
    end

    # @param [Object] owner 仮パラメータ値がSymbolだった場合にメソッドのレシーバとなるオブジェクト
    # @return [Proc] 仮パラメータ実体化手続き
    def materialization_curry(owner, *args)
      lambda do |key, _value|
        case _value
        when Proc then _value.call(self, owner, *args)
        when Symbol then owner.send(_value, self, *args)
        else _value
        end
      end
    end
  end
end
