module RouteLink
  class Engine < ::Rails::Engine

    initializer "route_link: configure route_link" do
      Rails.configuration.x.route_link = Config.new.configure do |config|
        config.route_params(
          query: {},
        )
        config.link_params(
          format: :path,
          i18n_params: {},
          html_attributes: {},
        )
      end
    end
  end
end
