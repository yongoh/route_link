module RouteLink
  module ParamsSet
    extend ActiveSupport::Concern

    module ClassMethods

      # @return [Hash<Symbol => Class<RouteLink::Params>>] 定義済みのアクセサメソッド名とパラメータオブジェクトクラスのペアのハッシュ
      def params
        @params ||= {}.freeze
      end

      # パラメータオブジェクトへのアクセサメソッドを作成
      # @param [Hash<Symbol => Class<RouteLink::Params>>] classes メソッド名とパラメータオブジェクトクラスのペアのハッシュ
      def param(classes)
        @params = params.merge(classes.symbolize_keys).freeze

        classes.each do |key, klass|
          attr_writer key

          # パラメータオブジェクトへのアクセサメソッド
          # @overload #{key}()
          #   @return [RouteLink::Params] `key`の名前に対応するパラメータオブジェクト
          # @overload #{key}(other)
          #   @param [Object] other マージするパラメータ
          #   @return [RouteLink::Params] `key`の名前に対応するパラメータオブジェクトと引数をマージした結果
          define_method(key) do |other = nil, &block|
            params = instance_variable_get("@#{key}") || klass.new
            params = params.merge(other, &block) if !other.nil? || block
            send("#{key}=", params)
          end
        end
      end

      # @overload merge(arg, arg, ...)
      #   @param [Object] arg パラメータオブジェクトもしくはマージする値
      #   @raise [ArgumentError] アクセサメソッドの数より多い引数を渡した場合に発生
      # @overload merge(others)
      #   @param [Hash<key => Object>] others アクセサメソッド名のキーとマージする値のペアのハッシュ
      # @overload merge(other)
      #   @param [RouteLink::ParamsSet] other 同じアクセサメソッド群を持つオブジェクト
      # @return [Hash<key => Object>] アクセサメソッド名のキーとパラメータオブジェクトに変換可能な値のペアのハッシュ
      def to_h(others, *args)
        if args.any?
          # 複数の引数を渡した場合
          args.unshift(others)
          raise_argument_error(args.size) if args.size > params.size
          args.each_with_index.map{|other, i| [params.keys[i], other] }.to_h
        elsif others.is_a?(Enumerable) && params.keys.any?{|key| others.to_h.has_key?(key) }
          # アクセサメソッド名のキーとマージする値のペアのハッシュを渡した場合
          others.to_h
        elsif params.keys.all?{|key| others.respond_to?(key) }
          # 同じアクセサメソッド群を持つオブジェクトを渡した場合
          params.keys.map{|key| [key, others.public_send(key)] }.to_h
        else
          # それ以外
          {params.keys.first => others}
        end
      end

      private

      def raise_argument_error(args_size)
        raise ArgumentError, "wrong number of arguments (given #{args_size}, expected 1..#{params.size})"
      end
    end

    def initialize_copy(obj)
      super(obj)
      merge!(*Array.new(self.class.params.size, {}))
    end

    # @yield [params_set] 設定用ブロック
    # @yieldparam [RouteLink::ParamsSet] params_set 自身
    # @return [RouteLink::ParamsSet] 設定済みの自身
    def configure
      yield(self)
      self
    end

    # @return [RouteLink::ParamsSet] パラメータ群をマージした新しいインスタンス
    # @see ::to_h
    def merge(*args, &block)
      dup.merge!(*args, &block)
    end

    # 各パラメータを自身に破壊的にマージ
    # @see ::to_h
    def merge!(*args, &block)
      self.class.to_h(*args).each do |key, other|
        public_send(key, other, &block)
      end
      self
    end

    # @return [RouteLink::ParamsSet] 実体化したパラメータ値を持つ新しいインスタンス
    def materialize(*args, &block)
      merge(_materialize(*args, &block))
    end

    private

    # @return [Hash<Symbol => Object>] 値を実体化したパラメータオブジェクト群
    def _materialize(*args, &block)
      curry = materialization_curry(*args, &block)
      self.class.params.keys.map do |key|
        [key, public_send(key).materialize(&curry)]
      end.to_h
    end

    # @return [Proc] 仮パラメータ実体化手続き
    def materialization_curry(*args, &block)
      lambda{|key, _value| _value }
    end
  end
end
