module RouteLink
  class RouteParams < Params

    class << self

      def class_get(arg)
        if arg.is_a?(Params)
          arg.class
        elsif arg.respond_to?(:model_name) || arg.is_a?(Symbol)
          Resourceful::RouteParams
        elsif arg.is_a?(String) || arg.is_a?(URI::Generic)
          Uri::RouteParams
        elsif arg.respond_to?(:keys)
          class_get(arg.keys)
        elsif arg.is_a?(Enumerable)
          subclasses.max_by{|klass| (klass.keys(true) & arg).size }
        else
          RouteParams
        end
      end

      # @param [Hash] options URLクエリおよびその他のパラメータ
      # @return [RouteLink::RouteParams] このクラスのインスタンス
      def query_build(**options)
        keys = subclasses.reduce([]){|tmp, klass| tmp | klass.keys(true) }
        build(options.merge(query: options.slice!(*keys)))
      end

      private

      def subclasses
        [RouteParams, Resourceful::RouteParams, Uri::RouteParams]
      end
    end

    param *(URI::Generic.component - [:path, :query])
    param :query, merge_method: :url_parameter_merge

    %i(path url).each do |method_type|

      # @param [ActionView::Base] helpers ビューコンテキスト
      # @return [String] パス/URL
      define_method(method_type) do |helpers|
        raise NotImplementedError, "You must implement ##{__method__}"
      end
    end

    # @return [Hash] ルーティング関係の情報を持つ訳文パラメータ
    def i18n_params
      keys.map{|key| [key, proc{ public_send(key) }] }.to_h
    end
  end
end
