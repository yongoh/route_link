module RouteLink
  class LinkParams < Params

    FORMAT_KEY_SEPARATOR = ":".freeze

    class << self

      # @param [ActionView::Base] helpers ビューコンテキスト
      # @param [String,Symbol] format フォーマット文字列もしくはフォーマットキー
      # @param [Hash] options HTML属性値およびその他のパラメータ
      # @yield [] 内容ブロック
      # @return [RouteLink::LinkParams] このクラスのインスタンス
      def link_build(helpers, format = nil, **options, &content_proc)
        format ||= helpers.capture(&content_proc) if block_given?
        new.merge!(options.merge(
          format: format,
          html_attributes: options.slice!(*keys(true)),
        ))
      end

      # 名前空間付き翻訳メソッド
      # @param [String] separator 訳文キーのセパレータ文字
      # @param [Array] keys 訳文キーの配列
      # @param [Hash] i18n_params 訳文パラメータ
      # @return [String] フォーマット済みの文字列
      def translate(separator, *keys, **i18n_params)
        I18n.t(keys.join(separator), **i18n_params, default: proc{
          keys.shift
          if keys.empty?
            I18n.t(i18n_params)
          else
            translate(separator, *keys, i18n_params)
          end
        })
      end

      # @param [String] fmtstr フォーマット文字列
      # @param [Hash] i18n_params 訳文パラメータ
      # @return [String] フォーマット済み文字列
      def string_format(fmtstr, **i18n_params)
        fmtstr.class.new(I18n.t(default: fmtstr, **i18n_params))
      end

      # @param [Symbol] format_key フォーマット文字列の訳文キー
      # @param [Hash] i18n_params 訳文パラメータ
      # @return [String] フォーマット済み文字列
      def locale_format(format_key, **i18n_params)
        keys = format_key.to_s.split(FORMAT_KEY_SEPARATOR)
        LinkParams.translate(
          FORMAT_KEY_SEPARATOR,
          *keys,
          scope: ROUTE_NAME_SCOPE + [:formats],
          default: proc{ i18n_param_get(keys.last, i18n_params) },
          **i18n_params,
        )
      end

      # @param [Symbol] param_key 訳文パラメータキー
      # @param [Hash] i18n_params 訳文パラメータ
      # @return [String] 訳文パラメータ値
      def i18n_param_get(param_key, **i18n_params)
        I18n.t(default: "%{#{param_key}}", **i18n_params)
      end
    end

    # @!attribute [rw] format
    #   @return [String] ルーティング名のフォーマット文字列
    # @!attribute [rw] i18n_params
    #   @return [Hash] ルーティング名のフォーマットパラメータ
    # @!attribute [rw] html_attributes
    #   @return [Hash] HTML属性
    param :format
    param :i18n_params, merge_method: :merge
    param :html_attributes, merge_method: :html_attribute_merge

    # @return [String] 翻訳したルーティング名
    def name
      case format
      when String then LinkParams.string_format(format, i18n_params)
      when Symbol then LinkParams.locale_format(format, i18n_params)
      else raise TypeError, "no implicit conversion of #{format.class} into String"
      end
    end

    # @return [RouteLink::LinkParams] 自身と渡したパラメータをマージしたリンクパラメータ
    # @see ::link_build
    def link_merge(*args, &block)
      merge(self.class.link_build(*args, &block))
    end
  end
end
