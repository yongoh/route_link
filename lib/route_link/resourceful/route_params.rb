module RouteLink
  module Resourceful

    ACTION_KEY_SEPARATOR = "#".freeze

    class RouteParams < RouteLink::RouteParams

      class << self

        def to_h(arg)
          case arg
          when Proc then {_controller: arg}
          when Symbol then {action: arg}
          else
            if arg.respond_to?(:model_name)
              {controller: arg}
            else
              super
            end
          end
        end
      end

      # @!attribute [rw] controller
      #   @return [Object] コントローラを表すオブジェクト
      # @!attribute [rw] action
      #   @return [Symbol] アクション名
      # @!attribute [rw] uri
      param :controller, :action

      %i(path url).each do |method_name|

        # @param [ActionView::Base] helpers ビューコンテキスト
        # @return [String] パス/URL
        # FIXME: 複数形リソースを単数形の名前で設定した場合（パスメソッドの真ん中に'_index'がつく）には対応していない
        define_method(method_name) do |helpers|
          components = controller.is_a?(String) ? controller.split("/") : Array(controller).dup
          components.push(route_key_of(components.pop)).map!{|c| c.respond_to?(:to_sym) ? c.to_sym : c }
          action = [:index, :show, :destroy].include?(self.action) ? nil : self.action
          url_params = uri_components.merge(query || {})
          ActionDispatch::Routing::PolymorphicRoutes::HelperMethodBuilder.polymorphic_method(helpers, components, action, method_name, url_params)
        end
      end

      # @return [String] 翻訳したコントローラ名
      def human_controller_name
        I18n.t(controller_key, scope: ROUTE_NAME_SCOPE + [:controllers], default: proc{ human_model_name })
      end

      # @return [String] コントローラ名から推測して翻訳したモデル名
      def human_model_name
        if controller.respond_to?(:model_name)
          controller.model_name.human
        else
          I18n.t(
            controller_key.singularize,
            scope: [ActiveRecord::Base.i18n_scope, :models],
            default: proc{ controller_key.humanize },
          )
        end
      end

      # @return [String] 翻訳したアクション名
      def human_action_name
        LinkParams.translate(
          ACTION_KEY_SEPARATOR,
          controller_key,
          action,
          scope: ROUTE_NAME_SCOPE + [:actions],
          default: proc{ action.to_s.humanize },
        )
      end

      def i18n_params
        super.merge(
          Object: proc{ human_controller_name },
          action: proc{ human_action_name },
        )
      end

      private

      # @return [Hash] パス部分とURIクエリを除くURI構成要素のハッシュ
      def uri_components
        h = (URI::Generic.component - [:path, :query]).map{|key| [replace_uri_component_key(key), send(key)] }.to_h
        h[:user], h[:password] = h.delete(:userinfo).to_s.split(":")
        h.compact
      end

      # `URI::Generic.component`基準のURI構成要素名を`#polymorphic_method`および`#url_for`用に変換
      def replace_uri_component_key(key)
        case key
        when :scheme then :protocol
        when :fragment then :anchor
        else key
        end
      end

      # @param [ActiveRecord::Base,Class<ActiveRecord::Base>,String] component コントローラを表すオブジェクト（単体）
      # @return [String] アクションに応じて単数or複数形に整形したコントローラ名
      def route_key_of(component)
        if component.respond_to?(:model_name)
          case action
            when :index, :search then component.model_name.route_key
            when :new then component.model_name.singular_route_key
            when :show, :edit, :destroy then component.respond_to?(:id) ? component : component.model_name.singular_route_key
            else component
          end
        else
          case action
            when :index, :search then component.to_s.pluralize
            when :new, :show, :edit, :destroy then component.to_s.singularize
            else component
          end
        end
      end

      # @return [String] コントローラ名の訳文キー
      def controller_key
        if controller.respond_to?(:model_name)
          controller.model_name.route_key
        else
          controller.to_param
        end
      end
    end
  end
end
