require 'route_link/engine'

require 'route_link/params'
require 'route_link/route_params'
require 'route_link/resourceful/route_params'
require 'route_link/uri/route_params'
require 'route_link/link_params'
require 'route_link/params_set'
require 'route_link/link_builder'
require 'route_link/config'

module RouteLink

  # ルーティング名の訳文キーのスコープ
  ROUTE_NAME_SCOPE = [:routes].freeze

  class << self

    # @param [String] path パス
    # @return [String] パス名
    def human_path_name(path)
      I18n.t(path, scope: ROUTE_NAME_SCOPE + [:paths], default: proc{ path })
    end
  end
end
