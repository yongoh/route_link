module RouteLink
  class LocaleGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    argument :locales, type: :array, default: %i(en ja), banner: "locale locale"

    def copy_locale_files
      locales.each do |locale|
        source = "#{locale}.yml"
        destination = "config/locales/routes/#{locale}.yml"

        if File.exist?(File.expand_path(source, self.class.source_root))
          copy_file source, destination
        else
          copy_file "en.yml", destination
          gsub_file destination, /^en:/, "#{locale}:"
        end
      end
    end
  end
end
