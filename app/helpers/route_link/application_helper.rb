module RouteLink
  module ApplicationHelper

    # @return [RouteLink::LinkBuilder] リンクビルダー
    # @see RouteLink::LinkBuilder#merge!
    def route(*args)
      LinkBuilder.build(self).merge!(*args)
    end

    # @return [ActiveSupport::SafeBuffer] リンク要素
    # @see RouteLink::LinkBuilder#link
    def route_link_to(*args, &block)
      LinkBuilder.build(self).materialize(self).link(*args, &block)
    end
  end
end
