require 'rails_helper'

describe RouteLink do
  describe "::human_path_name" do
    context "パスと同じ訳文キーがある場合" do
      it "は、訳文を返すこと" do
        expect(described_class.human_path_name("/foo/bar")).to eq("ふーばー")
      end
    end

    context "パスに対応する訳文キーがない場合" do
      it "は、パス文字列を返すこと" do
        expect(described_class.human_path_name("/missing_path")).to eq("/missing_path")
      end
    end
  end
end
