require 'rails_helper'

describe RouteLink::ApplicationHelper do
  describe "#route" do
    subject do
      h.route(
        {controller: Person, action: :index, query: {a: "A1", b: "B1"}},
        {format: "x=%{x} y=%{y}", i18n_params: {x: "X1", y: "Y1"}},
      )
    end

    it "は、リンクビルダーを返すこと" do
      is_expected.to be_a(RouteLink::LinkBuilder).and have_attributes(
        route_params: have_attributes(controller: Person, action: :index, query: match(a: "A1", b: "B1")),
        link_params: have_attributes({format: "x=%{x} y=%{y}", i18n_params: match(x: "X1", y: "Y1")}),
      )
    end
  end

  describe "#route_link_to" do
    subject do
      h.route_link_to(
        "x=%{x} y=%{y}",
        {controller: Person, action: :new, a: "A1", b: "B1"},
        {id: "my-id", class: "my-class", i18n_params: {x: "X1", y: "Y1"}},
      )
    end

    it "は、リンク要素を返すこと" do
      is_expected.to have_tag("a#my-id.my-class", with: {href: "/people/new?a=A1&b=B1"}, text: "x=X1 y=Y1")
    end
  end
end
