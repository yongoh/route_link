require 'rails_helper'

describe RouteLink::Uri::RouteParams do
  describe "::to_h" do
    subject{ described_class.to_h(arg) }

    shared_examples_for "uri hash" do
      it "は、URIの各要素に分解したハッシュを返すこと" do
        is_expected.to match(
          fragment: nil,
          host: "foobar",
          path_component: "/hoge/piyo",
          port: 80,
          query: {a: "A1", b: "B1"},
          scheme: "http",
          userinfo: nil,
        )
      end
    end

    context "キー`:query`を含むハッシュを渡した場合" do
      let(:arg){ {x: "X", y: "Y", query: "a=A1&b=B1"} }

      it "は、キー`:query`をハッシュに変換した値を持つハッシュを返すこと" do
        is_expected.to match(x: "X", y: "Y", query: {a: "A1", b: "B1"})
      end
    end

    context "配列の配列を渡した場合" do
      let(:arg){ [[:a, "A"], [:b, "B"]] }

      it "は、ハッシュに変換して返すこと" do
        is_expected.to match(a: "A", b: "B")
      end
    end

    context "URIオブジェクトを渡した場合" do
      let(:arg){ URI.parse("http://foobar/hoge/piyo?a=A1&b=B1") }
      it_behaves_like "uri hash"
    end

    context "文字列を渡した場合" do
      let(:arg){ "http://foobar/hoge/piyo?a=A1&b=B1" }
      it_behaves_like "uri hash"
    end

    context "それ以外を渡した場合" do
      let(:arg){ 123 }
      it{ expect{ subject }.to raise_error(NoMethodError, /\Aundefined method `to_h'/) }
    end
  end

  describe "#uri" do
    subject{ params.uri }

    context "URI情報を持つパラメータが無い場合" do
      let(:params){ described_class.new }

      it "は、空のURIオブジェクトを返すこと" do
        is_expected.to be_instance_of(URI::Generic).and have_attributes(
          fragment: nil,
          host: nil,
          path: "",
          port: nil,
          query: nil,
          scheme: nil,
          userinfo: nil,
          to_s: "",
        )
      end
    end

    context "URI情報を持つパラメータがある場合" do
      let(:params){ described_class.new.merge!(host: "foobar", path_component: "/hoge/piyo") }

      it "は、URIオブジェクトを返すこと" do
        is_expected.to be_instance_of(URI::Generic).and have_attributes(
          fragment: nil,
          host: "foobar",
          path: "/hoge/piyo",
          port: nil,
          query: nil,
          scheme: nil,
          userinfo: nil,
          to_s: "//foobar/hoge/piyo",
        )
      end
    end

    context "パラメータ`#scheme`がスキーマ情報を返す場合" do
      let(:params){ described_class.new.merge!(scheme: "http", host: "foobar", path_component: "/hoge/piyo") }

      it "は、スキーマに対応するクラスのURIオブジェクトを返すこと" do
        is_expected.to be_instance_of(URI::HTTP).and have_attributes(
          fragment: nil,
          host: "foobar",
          path: "/hoge/piyo",
          port: 80,
          query: nil,
          scheme: "http",
          userinfo: nil,
          to_s: "http://foobar/hoge/piyo",
        )
      end
    end

    context "パラメータ`#query`がURLクエリハッシュを返す場合" do
      let(:params){ described_class.new.merge!(host: "foobar", path_component: "/hoge/piyo", query: {a: "A1", b: "B1"}) }

      it "は、URLクエリを持つURIオブジェクトを返すこと" do
        is_expected.to be_instance_of(URI::Generic).and have_attributes(
          fragment: nil,
          host: "foobar",
          path: "/hoge/piyo",
          port: nil,
          query: "a=A1&b=B1",
          scheme: nil,
          userinfo: nil,
          to_s: "//foobar/hoge/piyo?a=A1&b=B1",
        )
      end
    end

    context "`#default_url_options`を持つオブジェクトを渡した場合" do
      subject{ params.uri(helpers) }

      let(:params){ described_class.new.merge!(path_component: "/hoge/piyo") }
      let(:helpers){ double("ActionView::Base", default_url_options: {scheme: "https", host: "www.example.com"}) }

      it "は、`#default_url_options`の値でURI情報を補完したURIオブジェクトを返すこと" do
        is_expected.to be_instance_of(URI::HTTPS).and have_attributes(
          fragment: nil,
          host: "www.example.com",
          path: "/hoge/piyo",
          port: 443,
          query: nil,
          scheme: "https",
          userinfo: nil,
          to_s: "https://www.example.com/hoge/piyo",
        )
      end
    end
  end

  describe "#path" do
    subject{ params.path(view_context) }

    context "URLクエリを持たない場合" do
      let(:params){ described_class.new.merge!(host: "foobar", path_component: "/hoge/piyo") }

      it "は、`#uri`のパス部分を返すこと" do
        is_expected.to eq("/hoge/piyo")
      end
    end

    context "URLクエリを持つ場合" do
      let(:params){ described_class.new.merge!(host: "foobar", path_component: "/hoge/piyo", query: {a: "A1", b: "B1"}) }

      it "は、URLクエリ付きパスを返すこと" do
        is_expected.to eq("/hoge/piyo?a=A1&b=B1")
      end
    end
  end

  describe "#url" do
    subject{ params.url(view_context) }

    context "URLクエリを持たない場合" do
      let(:params){ described_class.new.merge!(host: "foobar", path_component: "/hoge/piyo") }

      it "は、`#uri`のフルパスを返すこと" do
        is_expected.to eq("//foobar/hoge/piyo")
      end
    end

    context "URLクエリを持つ場合" do
      let(:params){ described_class.new.merge!(host: "foobar", path_component: "/hoge/piyo", query: {a: "A1", b: "B1"}) }

      it "は、クエリ付きフルパスを返すこと" do
        is_expected.to eq("//foobar/hoge/piyo?a=A1&b=B1")
      end
    end
  end

  describe "#human_path_name" do
    subject{ params.human_path_name }

    context "URIのパス部分と同じ訳文キーがある場合" do
      let(:params){ described_class.new.merge!(path_component: "/foo/bar", query: {a: "A1", b: "B1"}) }

      it "は、訳文を返すこと" do
        is_expected.to eq("ふーばー")
      end
    end

    context "パスに対応する訳文キーがない場合" do
      let(:params){ described_class.new.merge!(path_component: "/missing_path", query: {a: "A1", b: "B1"}) }

      it "は、パス文字列を返すこと" do
        is_expected.to eq("/missing_path")
      end
    end
  end
end
