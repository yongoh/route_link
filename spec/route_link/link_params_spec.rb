require 'rails_helper'

describe RouteLink::LinkParams do
  describe "::link_build" do
    context "第2引数にフォーマットを渡した場合" do
      subject{ described_class.link_build(view_context, "Format arg") }

      it "は、`#format`が渡したものを返すインスタンスを生成すること" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          format: "Format arg",
          i18n_params: be_nil,
          html_attributes: {},
        )
      end
    end

    context "第2引数にハッシュを渡した場合" do
      subject{ described_class.link_build(view_context, {class: "my-class", id: "my-id"}) }

      it "は、`#html_attributes`が渡したものを返すインスタンスを生成すること" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          format: be_nil,
          i18n_params: be_nil,
          html_attributes: match(class: "my-class", id: "my-id"),
        )
      end

      context "オプション`:i18n_params`を渡した場合" do
        subject{ described_class.link_build(view_context, i18n_params: {a: "A1", b: "B1"}) }

        it "は、`#i18n_params`が渡したものを返すインスタンスを生成すること" do
          is_expected.to be_instance_of(described_class).and have_attributes(
            format: be_nil,
            i18n_params: match(a: "A1", b: "B1"),
            html_attributes: {},
          )
        end
      end

      context "オプション`:_i18n_params`を渡した場合" do
        subject{ described_class.link_build(view_context, _i18n_params: {a: "A1", b: "B1"}) }

        it "は、`#i18n_params`が渡したものを返すインスタンスを生成すること" do
          is_expected.to be_instance_of(described_class).and have_attributes(
            format: be_nil,
            i18n_params: match(a: "A1", b: "B1"),
            html_attributes: {},
          )
        end
      end

      context "オプション`:html_attributes`を渡した場合" do
        subject{ described_class.link_build(view_context, html_attributes: {class: "my-class", id: "my-id"}) }

        it "は、渡したものは無視されること" do
          is_expected.to be_instance_of(described_class).and have_attributes(
            format: be_nil,
            i18n_params: be_nil,
            html_attributes: {},
          )
        end
      end
    end

    context "第2引数にフォーマット・第3引数にキー`:format`を含むハッシュを渡した場合" do
      subject do
        described_class.link_build(
          view_context,
          "Format arg",
          {class: "my-class", id: "my-id", format: "Format param"},
        )
      end

      it "は、第2引数を優先したフォーマットパラメータを持つインスタンスを生成すること" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          format: "Format arg",
          i18n_params: be_nil,
          html_attributes: match(class: "my-class", id: "my-id"),
        )
      end
    end

    context "ブロックを渡した場合" do
      context "第2引数を渡さない場合" do
        subject{
          described_class.link_build(view_context) do
            h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
            h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
          end
        }

        it "は、ブロック内で作成した内容をフォーマットパラメータとして持つインスタンスを生成すること" do
          is_expected.to be_instance_of(described_class).and have_attributes(
            format: '<div id="shobon">(´･ω･｀)</div><span class="boon">（＾ω＾）</span>',
            i18n_params: be_nil,
            html_attributes: {},
          )
        end
      end

      context "第2引数にフォーマットを渡した場合" do
        subject{
          described_class.link_build(view_context, "Format arg") do
            h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
            h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
          end
        }

        it "は、第2引数を優先したフォーマットパラメータを持つインスタンスを生成すること" do
          is_expected.to be_instance_of(described_class).and have_attributes(
            format: "Format arg",
            i18n_params: be_nil,
            html_attributes: {},
          )
        end
      end
    end
  end

  describe "::translate" do
    subject do
      described_class.translate(described_class::FORMAT_KEY_SEPARATOR, *keys, i18n_params)
    end

    let(:i18n_params){ {scope: RouteLink::ROUTE_NAME_SCOPE + [:formats]} }

    context "訳文キー'aaa:bbb:ccc'に対応するキーの配列を渡した場合" do
      let(:keys){ %i(aaa bbb ccc) }

      it "訳文キー'aaa:bbb:ccc'の訳文を返すこと" do
        is_expected.to eq("ABC")
      end
    end

    context "訳文キー'aaa:bbb:ccc'に対応せず'bbb:ccc'に対応するキーの配列を渡した場合" do
      let(:keys){ %i(missing_key bbb ccc) }

      it "キー'bbb:ccc'の訳文を返すこと" do
        is_expected.to eq("BC")
      end
    end

    context "訳文キー'aaa:bbb:ccc','bbb:ccc'に対応せず'ccc'に対応するキーの配列を渡した場合" do
      let(:keys){ %i(missing_key missing_key ccc) }

      it "キー'ccc'の訳文を返すこと" do
        is_expected.to eq("C")
      end
    end

    context "最後のキーが対応する訳文キーが無い場合" do
      let(:keys){ %i(aaa bbb missing_key) }

      context "オプション`:default`を渡さない場合" do
        it "は、訳文キーが見つからないことを示す文字列を返すこと" do
          is_expected.to eq("translation missing: ja.routes.formats.missing_key")
        end
      end

      context "オプション`:default`を渡した場合" do
        before do
          i18n_params.merge!(default: proc{ "Key not found!" })
        end

        it "は、デフォルト値を返すこと" do
          is_expected.to eq("Key not found!")
        end
      end
    end
  end

  let(:params){ described_class.new.merge!(i18n_params: {x: "X1", y: "Y1"}) }

  describe "#name" do
    subject{ params.name }

    context "パラメータ`#format`が`nil`を返す場合" do
      before do
        allow(params).to receive(:format).and_return(nil)
      end

      it{ expect{ subject }.to raise_error(TypeError) }
    end

    context "パラメータ`#format`がフォーマット文字列を返す場合" do
      before do
        allow(params).to receive(:format).and_return("x=%{x} y=%{y}")
      end

      it "は、`#i18n_params`を訳文パラメータとして式展開した文字列を返すこと" do
        is_expected.to eq("x=X1 y=Y1")
      end
    end

    context "パラメータ`#format`がシンボルを返す場合" do
      context "キー'routes/formats/{format_key}'の訳文がある場合" do
        before do
          allow(params).to receive(:format).and_return(:boon)
        end

        it "は、その訳文でフォーマットした文字列を返すこと" do
          is_expected.to eq("（＾ω＾）")
        end
      end

      context "同じ名前のキーの訳文パラメータがある場合" do
        before do
          allow(params).to receive(:format).and_return(:x)
        end

        it "は、その訳文パラメータを返すこと" do
          is_expected.to eq("X1")
        end
      end

      context "どちらも存在する場合" do
        before do
          allow(params).to receive(:format).and_return(:boon)
          allow(params).to receive(:i18n_params).and_return({boon: "ブーン"})
        end

        it "は、'routes/formats/{format_key}'の訳文でフォーマットした文字列を返すこと" do
          is_expected.to eq("（＾ω＾）")
        end
      end

      context "どれも存在しない場合" do
        before do
          allow(params).to receive(:format).and_return(:missing_format_key)
        end

        it "は、訳文が見つからなかったことを示す例外を発生させること" do
          expect{ subject }.to raise_error(I18n::MissingInterpolationArgument, /\Amissing interpolation argument :missing_format_key/)
        end
      end
    end

    context "パラメータ`#format`がHTML要素を返す場合" do
      before do
        allow(params).to receive(:format).and_return(div)
      end

      let(:div){ h.content_tag(:div, "(´･ω･%{x}｀)", id: "shobon") }

      it "は、フォーマット済みのHTML要素を返すこと" do
        is_expected.to be_instance_of(ActiveSupport::SafeBuffer).and have_tag("div#shobon", text: "(´･ω･X1｀)")
      end
    end

    context "パラメータ`#format`がそれ以外のオブジェクトを返す場合" do
      before do
        allow(params).to receive(:format).and_return(123)
      end

      it{ expect{ subject }.to raise_error(TypeError, "no implicit conversion of Integer into String") }
    end
  end
end
