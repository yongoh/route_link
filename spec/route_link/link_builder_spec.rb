require 'rails_helper'

describe RouteLink::LinkBuilder do
  describe "::link_build" do
    context "第2引数にルーティングパラメータ・第3引数にリンクパラメータを渡した場合" do
      subject do
        described_class.link_build(
          view_context,
          {a: "A1", b: "B1", controller: Person, action: :index},
          {class: "my-class", id: "my-id", i18n_params: {x: "X1", y: "Y1"}, html_attributes: {id: "my-id-2", for: "my-for-2"}},
        )
      end

      it "は、渡した各パラメータを持つリンクビルダーを生成すること" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          route_params: have_attributes(
            query: match(a: "A1", b: "B1"),
            controller: Person,
            action: :index,
          ),
          link_params: have_attributes(
            i18n_params: match(x: "X1", y: "Y1"),
            html_attributes: match(class: "my-class", id: "my-id"),
          ),
        )
      end
    end

    context "第2引数に文字列を渡した場合" do
      subject{ described_class.link_build(view_context, "Format arg") }

      it "は、渡した文字列をフォーマットパラメータとして持つリンクビルダーを返すこと" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          link_params: have_attributes(
            format: "Format arg",
          ),
        )
      end
    end

    context "第2引数にSymbolを渡した場合" do
      subject{ described_class.link_build(view_context, :format_arg) }

      it "は、第1引数をフォーマットパラメータとして持つリンクビルダーを返すこと" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          link_params: have_attributes(
            format: :format_arg,
          ),
        )
      end
    end

    context "第2引数にフォーマット・第3引数にルーティングパラメータ・第4引数にリンクパラメータを渡した場合" do
      subject do
        described_class.link_build(
          view_context,
          "Format arg",
          {a: "A1", b: "B1"},
          {class: "my-class", id: "my-id"},
        )
      end

      it "は、渡した各パラメータを持つリンクビルダーを生成すること" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          route_params: have_attributes(
            query: match(a: "A1", b: "B1"),
          ),
          link_params: have_attributes(
            format: "Format arg",
            html_attributes: match(class: "my-class", id: "my-id"),
          ),
        )
      end
    end
  end

  let(:builder){ described_class.new(view_context) }
  let(:route_params){ "https://example.com/people/1" }
  let(:link_params){ {} }

  before do
    builder.merge!(route_params, link_params)
  end

  describe "#name" do
    let(:link_params){
      {
        format: "scheme=%{scheme} host=%{host} path_component=%{path_component}",
        i18n_params: {host: "Host2", path_component: "Path2"},
        html_attributes: {id: "my-id", class: "my-class"},
      }
    }

    context "引数・ブロックを渡さない場合" do
      subject{ builder.name }

      it "は、リンクテキストを返すこと" do
        is_expected.to eq("scheme=https host=Host2 path_component=Path2")
      end
    end

    context "第1引数にフォーマット文字列を渡した場合" do
      subject{ builder.name("scheme(%{scheme}) host(%{host}) path_component(%{path_component})") }

      it "は、渡したフォーマットで式展開したリンクテキストを返すこと" do
        is_expected.to eq("scheme(https) host(Host2) path_component(Path2)")
      end
    end

    context "第1引数に訳文パラメータを渡した場合" do
      subject{ builder.name({path_component: "Path3"}) }

      it "は、渡した訳文パラメータで式展開したリンクテキストを返すこと" do
        is_expected.to eq("scheme=https host=Host2 path_component=Path3")
      end
    end

    context "第1引数にフォーマット・第2引数に訳文パラメータを渡した場合" do
      subject{ builder.name("scheme(%{scheme}) host(%{host}) path_component(%{path_component})", {path_component: "Path3"}) }

      it "は、渡したフォーマット・訳文パラメータで式展開したリンクテキストを返すこと" do
        is_expected.to eq("scheme(https) host(Host2) path_component(Path3)")
      end
    end

    context "ブロックを渡した場合" do
      subject do
        builder.name do
          h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
          h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
        end
      end

      it "は、ブロック内で作成した要素を返すこと" do
        is_expected.to eq('<div id="shobon">(´･ω･｀)</div><span class="boon">（＾ω＾）</span>')
      end
    end
  end

  describe "#path" do
    context "引数を渡さない場合" do
      subject{ builder.path }

      it "は、ルーティングへのパスを返すこと" do
        is_expected.to eq("/people/1")
      end
    end

    context "第1引数にURLクエリを渡した場合" do
      subject{ builder.path({a: "A", b: {x: "X"}}) }

      it "は、マージしたルーティングへのパスを返すこと" do
        is_expected.to eq("/people/1?a=A&b%5Bx%5D=X")
      end
    end
  end

  describe "#url" do
    subject{ builder.url }

    it "は、ルーティングへのURL（フルパス）を返すこと" do
      is_expected.to eq("https://example.com/people/1")
    end
  end

  describe "#link" do
    before do
      builder.link_params(format: "(´･ω･｀)")
    end

    context "引数・ブロックを渡さない場合" do
      it "は、リンク要素を返すこと" do
        expect(builder.link).to have_tag("a", with: {href: "/people/1"}, text: "(´･ω･｀)")
      end
    end

    context "引数を渡した場合" do
      subject do
        builder.link("(・∀・)", {a: "A", b: "B"}, {class: "my-class", id: "my-id"})
      end

      it "は、第1引数を内容・第2引数をURLクエリ・第3引数をHTML属性として持つリンク要素を返すこと" do
        is_expected.to have_tag("a.my-class#my-id", with: {href: "/people/1?a=A&b=B"}, text: "(・∀・)")
      end
    end

    context "ブロックを渡した場合" do
      subject do
        builder.link do
          h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
          h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
        end
      end

      it "は、ブロック内で作成した内容を持つリンク要素を返すこと" do
        is_expected.to have_tag("a", with: {href: "/people/1"}){
          with_tag("div#shobon", text: "(´･ω･｀)")
          with_tag("span.boon", text: "（＾ω＾）")
        }
      end
    end
  end

  describe "#materialize" do
    subject do
      builder.materialize(*arguments)
    end

    let(:arguments){ 3.times.map{|n| double("Argument #{n}") } }
    let(:result){ double("Result") }

    context "'_'付きパラメータ値が手続きオブジェクトの場合" do
      it "は、手続きに自身と引数を渡すこと" do
        builder.link_params._format = proc do |*args|
          expect(args).to match([
            equal(builder),
            *arguments.map{|arg| equal(arg) },
          ])
        end
        subject
      end

      it "は、手続きの結果をパラメータ値として持つ新しいリンクビルダーを返すこと" do
        builder.link_params._format = proc{ result }

        is_expected.to be_a(described_class)
        is_expected.not_to equal(builder)
        expect(subject.link_params.format).to equal(result)
      end

      it "は、自身のパラメータ値を変化させないこと" do
        builder.link_params._format = materialization_proc = proc{}
        subject
        expect(builder.link_params.format).to equal(materialization_proc)
      end
    end

    context "'_'付きパラメータ値がSymbolの場合" do
      before do
        builder.link_params._format = :foobar
      end

      it "は、最初の引数が持つその名前のメソッドに自身と残りの引数を渡すこと" do
        expect(arguments[0]).to receive(:foobar).with(
          equal(builder),
          equal(arguments[1]),
          equal(arguments[2]),
        )
        subject
      end

      it "は、メソッドの戻り値をパラメータ値として持つ新しいリンクビルダーを返すこと" do
        allow(arguments[0]).to receive(:foobar).and_return(result)

        is_expected.to be_a(described_class)
        is_expected.not_to equal(builder)
        expect(subject.link_params.format).to equal(result)
      end

      it "は、自身のパラメータ値を変化させないこと" do
        allow(arguments.first).to receive(:foobar)
        subject
        expect(builder.link_params.format).to equal(:foobar)
      end
    end

    context "'_'付きパラメータ値がそれ以外の場合" do
      before do
        builder.link_params._format = result
      end

      it "は、その値をパラメータ値として持つ新しいリンクビルダーを返すこと" do
        is_expected.to be_a(described_class)
        is_expected.not_to equal(builder)
        expect(subject.link_params.format).to equal(result)
      end

      it "は、自身のパラメータ値を変化させないこと" do
        subject
        expect(builder.link_params.format).to equal(result)
      end
    end
  end
end
