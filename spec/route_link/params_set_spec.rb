require 'rails_helper'

describe RouteLink::ParamsSet do
  let(:owner){ owner_class.new }
  let(:owner_class){ Class.new.include(described_class) }

  before do
    owner_class.param(
      foo: foo_class,
      bar: bar_class,
      "baz" => baz_class,
    )
  end

  %w(foo bar baz).each do |xxx|
    let("#{xxx}_class"){ Class.new(RouteLink::Params) }
  end

  %w(x y z).each do |xxx|
    let("arg_#{xxx}"){ double("Argument #{xxx}") }
    let("result_#{xxx}"){ double("Result #{xxx}") }
  end

  describe "::param" do
    it "は、渡したキーと値を`::params`に追加すること" do
      expect(owner_class.params).to match(
        foo: equal(foo_class),
        bar: equal(bar_class),
        baz: equal(baz_class),
      )
    end

    it "は、渡したキーの名前のメソッドを定義すること" do
      expect(owner.public_methods).to include(:foo, :bar, :baz)
    end

    describe "生成したアクセサメソッド" do
      context "引数なしの場合" do
        subject{ owner.foo }

        it "は、対応するパラメータオブジェクトを返すこと" do
          is_expected.to be_instance_of(foo_class)
        end

        it "は、結果をメモ化すること" do
          is_expected.to equal(owner.foo)
        end
      end

      context "引数を渡した場合" do
        subject{ owner.foo(arg_x) }

        before do
          allow(owner.foo).to receive(:merge).and_return(result_x)
        end

        it "は、パラメータオブジェクトの`#merge`に引数を移譲し、その結果を返すこと" do
          expect(owner.foo).to receive(:merge).with(arg_x)
          is_expected.to equal(result_x)
        end

        it "は、結果をメモ化すること" do
          foo = owner.foo
          is_expected.not_to equal(foo)
          is_expected.to equal(owner.foo)
        end
      end
    end
  end

  describe "#dup" do
    subject{ owner.dup }

    before do
      allow(owner.foo).to receive(:merge).and_return(result_x)
      allow(owner.bar).to receive(:merge).and_return(result_y)
      allow(owner.baz).to receive(:merge).and_return(result_z)
    end

    it "は、コピー元のパラメータオブジェクトのマージ結果を持つコピーを生成すること" do
      is_expected.not_to equal(owner)
      is_expected.to be_instance_of(owner_class).and have_attributes(
        foo: equal(result_x),
        bar: equal(result_y),
        baz: equal(result_z),
      )
    end

    it "は、自身のパラメータオブジェクトを変化させないこと" do
      expect{ subject }.not_to change{ owner.foo }
    end
  end

  describe "#merge!" do
    it "は、自身を返すこと" do
      expect(owner.merge!({})).to equal(owner)
    end

    context "第1引数に空のハッシュを渡した場合" do
      it "は、最初に定義したアクセサメソッドに引数を移譲すること" do
        expect(owner).to receive(:foo).with({})
        expect(owner).not_to receive(:bar)
        expect(owner).not_to receive(:baz)
        owner.merge!({})
      end
    end

    context "第1引数にアクセサメソッド名のキーを持つハッシュを渡した場合" do
      it "は、キーの名前のアクセサメソッドにその値を渡すこと" do
        expect(owner).to receive(:foo).with(arg_x)
        expect(owner).to receive(:bar).with(arg_y)
        expect(owner).not_to receive(:baz)
        owner.merge!(foo: arg_x, bar: arg_y)
      end
    end

    context "第1引数にアクセサメソッド名のキーを持たないハッシュを渡した場合" do
      it "は、最初に定義したアクセサメソッドに引数を移譲すること" do
        expect(owner).to receive(:foo).with(match(hoge: arg_x, piyo: arg_y))
        expect(owner).not_to receive(:bar)
        expect(owner).not_to receive(:baz)
        owner.merge!(hoge: arg_x, piyo: arg_y)
      end
    end

    context "2個以上の引数を渡した場合" do
      it "は、定義した順にアクセサメソッドに引数を移譲すること" do
        expect(owner).to receive(:foo).with(arg_x)
        expect(owner).to receive(:bar).with(arg_y)
        expect(owner).not_to receive(:baz)
        owner.merge!(arg_x, arg_y)
      end
    end

    context "アクセサメソッドの数より多い引数を渡した場合" do
      it "は、引数エラーを発生させること" do
        expect{
          owner.merge!(arg_x, arg_y, arg_z, arg_x)
        }.to raise_error(
          ArgumentError,
          "wrong number of arguments (given 4, expected 1..3)",
        )
      end
    end

    context "同じ名前のアクセサメソッドを全て持つオブジェクトを渡した場合" do
      let(:arg){ double("ParamsSet", foo: arg_x, bar: arg_y, baz: arg_z) }

      it "は、定義した順にアクセサメソッドに引数を移譲すること" do
        expect(owner).to receive(:foo).with(arg_x)
        expect(owner).to receive(:bar).with(arg_y)
        expect(owner).to receive(:baz).with(arg_z)
        owner.merge!(arg)
      end
    end

    context "同じ名前のアクセサメソッドを一部持つオブジェクトを渡した場合" do
      let(:arg){ double("ParamsSet", foo: arg_x, bar: arg_y) }

      it "は、最初に定義したアクセサメソッドに引数を移譲すること" do
        expect(owner).to receive(:foo).with(arg)
        expect(owner).not_to receive(:bar)
        expect(owner).not_to receive(:baz)
        owner.merge!(arg)
      end
    end
  end

  describe "#merge" do
    subject{ owner.merge(arg_x, arg_y, arg_z) }

    before do
      allow(owner.foo).to receive(:merge).and_return(result_x)
      allow(owner.bar).to receive(:merge).and_return(result_y)
      allow(owner.baz).to receive(:merge).and_return(result_z)
      expect(result_x).to receive(:merge).with(arg_x).and_return(result_x)
      expect(result_y).to receive(:merge).with(arg_y).and_return(result_y)
      expect(result_z).to receive(:merge).with(arg_z).and_return(result_z)
    end

    it "は、マージしたパラメータ群を持つ新しいインスタンスを返すこと" do
      is_expected.not_to equal(owner)
      is_expected.to be_instance_of(owner_class).and have_attributes(
        foo: equal(result_x),
        bar: equal(result_y),
        baz: equal(result_z),
      )
    end

    it "は、自身のパラメータオブジェクトを変化させないこと" do
      expect{ subject }.not_to change{ owner.foo }
    end
  end

  describe "#materialize" do
    subject do
      owner.materialize(arg_x, arg_y, arg_z)
    end

    before do
      foo_class.send(:param, :hoge)
      owner.foo._hoge = result_x
    end

    it "は、`#materialization_curry`に引数を移譲すること" do
      expect(owner).to receive(:materialization_curry).with(arg_x, arg_y, arg_z)
      subject
    end

    it "は、`#materialization_curry`が返す手続きの結果をパラメータ値として持つ新しいインスタンスを返すこと" do
      expect(owner).to receive(:materialization_curry).and_return(proc{ result_y })
      is_expected.to be_a(described_class)
      is_expected.not_to equal(owner)
      expect(subject.foo.hoge).to equal(result_y)
    end

    it "は、自身のパラメータ値を変化させないこと" do
      subject
      expect(owner.foo.hoge).to equal(result_x)
    end
  end
end
