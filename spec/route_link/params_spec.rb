require 'rails_helper'

describe RouteLink::Params do
  let(:params){ params_class_1.new(default: default) }
  let(:params_class_1){
    Class.new(described_class) do
      param :foo, :bar, :baz
    end
  }
  let(:params_class_2){
    Class.new(described_class) do
      param :foo, :bar, :baz
    end
  }
  let(:default){ double("Params") }

  let(:foo){ double("Foo") }
  let(:bar){ double("Bar") }
  let(:baz){ double("Baz") }
  let(:_foo){ double("_Foo") }
  let(:_bar){ double("_Bar") }
  let(:_baz){ double("_Baz") }

  let(:hash){ {foo: foo, _bar: _bar, baz: baz, _baz: _baz} }

  shared_examples_for "params of the same class as the receiver" do
    it "は、自身と同じクラスのパラメータオブジェクトを返すこと" do
      is_expected.to be_instance_of(params_class_1)
    end
  end

  shared_examples_for "params without default" do
    it "は、デフォルト値を持たないパラメータオブジェクトを返すこと" do
      is_expected.to have_attributes(default: be_nil)
    end
  end

  shared_examples_for "default is the receiver" do
    it "は、引数をデフォルト値に持つインスタンスを生成すること" do
      is_expected.to have_attributes(default: equal(arg))
    end

    it "は、渡したパラメータを持つインスタンスを生成すること" do
      is_expected.to have_attributes(
        foo: equal(foo),
        bar: equal(_bar),
        baz: equal(baz),
      )
      expect(subject.instance_variable_get(:@params)).to eq({})
      expect(subject.instance_variable_get(:@_params)).to eq({})
    end
  end

  describe "::build" do
    subject{ params_class_1.build(arg) }

    shared_examples_for "merged params" do
      it "は、渡したパラメータを持つパラメータオブジェクトを返すこと" do
        is_expected.to have_attributes(
          foo: equal(foo),
          bar: equal(_bar),
          baz: equal(baz),
        )
        expect(subject.instance_variable_get(:@params)).to match(
          foo: equal(foo),
          baz: equal(baz),
        )
        expect(subject.instance_variable_get(:@_params)).to match(
          bar: equal(_bar),
          baz: equal(_baz),
        )
      end
    end

    context "ハッシュを渡した場合" do
      let(:arg){ hash }

      it_behaves_like "params of the same class as the receiver"
      it_behaves_like "params without default"
      it_behaves_like "merged params"
    end

    context "配列の配列を渡した場合" do
      subject{ params_class_1.build(hash.to_a) }

      it_behaves_like "params of the same class as the receiver"
      it_behaves_like "params without default"
      it_behaves_like "merged params"
    end

    context "同じ種類のパラメータオブジェクトを渡した場合" do
      let(:arg){ params_class_1.new.merge!(hash) }

      it_behaves_like "params of the same class as the receiver"
      it_behaves_like "default is the receiver"
    end

    context "違う種類のパラメータオブジェクトを渡した場合" do
      let(:arg){ params_class_2.new.merge!(hash) }

      it_behaves_like "params of the same class as the receiver"
      it_behaves_like "default is the receiver"
    end

    context "全てのパラメータメソッドを持つオブジェクトを渡した場合" do
      let(:arg){ double("Params", foo: foo, bar: _bar, baz: baz) }

      it_behaves_like "params of the same class as the receiver"
      it_behaves_like "default is the receiver"
    end

    context "一部のパラメータメソッドを欠くオブジェクトを渡した場合" do
      let(:arg){ double("Params", foo: foo, bar: bar) }
      it{ expect{ subject }.to raise_error(RSpec::Mocks::MockExpectationError, '#<Double "Params"> received unexpected message :to_h with (no args)') }
    end

    context "`::class_get`が自身とは別のクラスを返す場合" do
      let(:arg){ hash }

      before do
        allow(params_class_1).to receive(:class_get).and_return(params_class_2)
      end

      it "は、そのクラスのインスタンスを生成すること" do
        is_expected.to be_instance_of(params_class_2)
      end

      it_behaves_like "params without default"
      it_behaves_like "merged params"
    end

    context "`::to_h`がパラメータハッシュを返す場合" do
      let(:arg){ hash }

      before do
        allow(params_class_1).to receive(:to_h).and_return({foo: "ふー", _bar: "バー", baz: "ばず", _baz: "バズ"})
      end

      it_behaves_like "params of the same class as the receiver"
      it_behaves_like "params without default"

      it "は、渡したパラメータを持つインスタンスを生成すること" do
        is_expected.to have_attributes(
          foo: eq("ふー"),
          bar: eq("バー"),
          baz: eq("ばず"),
        )
        expect(subject.instance_variable_get(:@params)).to match(
          foo: eq("ふー"),
          baz: eq("ばず"),
        )
        expect(subject.instance_variable_get(:@_params)).to match(
          bar: eq("バー"),
          baz: eq("バズ"),
        )
      end
    end
  end

  describe "#materialized_param" do
    subject{ params.materialized_param(:foo) }

    context "パラメータ値が`nil`以外の場合" do
      before do
        params.foo = foo
      end

      it "は、値をそのまま返すこと" do
        is_expected.to equal(foo)
      end
    end

    context "パラメータ値が`nil`の場合" do
      before do
        params.foo = nil
      end

      context "仮パラメータ値が`nil`以外の場合" do
        before do
          params._foo = _foo
        end

        context "ブロックを渡した場合" do
          it "は、パラメータ名と仮パラメータ値をブロック引数に渡すこと" do
            expect{|block| params.materialized_param(:foo, &block) }.to yield_with_args(:foo, equal(_foo))
          end

          it "は、ブロックの戻り値を返すこと" do
            expect(params.materialized_param(:foo){ bar }).to equal(bar)
          end
        end

        context "ブロック無しの場合" do
          it "は、仮パラメータ値を返すこと" do
            is_expected.to equal(_foo)
          end
        end
      end

      context "仮パラメータ値が`nil`の場合" do
        before do
          params._foo = nil
        end

        it{ is_expected.to be_nil }
      end
    end

    context "存在しないパラメータキーを渡した場合" do
      it "は、キーエラーを発生させること" do
        expect{ params.materialized_param(:undefined_key) }.to raise_error(KeyError, "key is undefined: :undefined_key")
      end
    end

    context "文字列のパラメータキーを渡した場合" do
      it "は、キーエラーを発生させること" do
        expect{ params.materialized_param("foo") }.to raise_error(KeyError, 'key is undefined: "foo"')
      end
    end
  end

  describe "#merged_param" do
    subject{ params.merged_param(:foo) }

    context "`#materialized_param`が`nil`を返す場合" do
      before do
        allow(params).to receive(:materialized_param).and_return(nil)
      end

      context "`#default`が`nil`を返す場合" do
        it{ is_expected.to be_nil }
      end

      context "`#default`がパラメータ名と同じ名前の属性メソッドを持つ場合" do
        before do
          allow(default).to receive(:foo).and_return(foo)
        end

        it "は、そのメソッドの結果を返すこと" do
          is_expected.to equal(foo)
        end
      end

      context "`#default`がパラメータ名と同じ名前のメソッドを持たない場合" do
        it{ is_expected.to be_nil }
      end
    end

    context "オプション`:merge_proc`に`nil`を渡した場合 (default)" do
      before do
        allow(params).to receive(:materialized_param).and_return(foo)
      end

      it{ is_expected.to equal(foo) }
    end

    context "オプション`:merge_proc`に手続きを渡した場合" do
      before do
        allow(params).to receive(:materialized_param).and_return(foo)
        allow(default).to receive(:foo).and_return(bar)
      end

      it "は、ブロック引数に自身とデフォルトのパラメータ値を渡すこと" do
        merge_proc = proc do |*args|
          expect(args).to match([equal(foo), equal(bar)])
        end
        params.merged_param(:foo, merge_proc: merge_proc)
      end

      it "は、手続きの戻り値を返すこと" do
        merge_proc = proc{ baz }
        expect(params.merged_param(:foo, merge_proc: merge_proc)).to equal(baz)
      end
    end

    context "オプション`:merge_method`を渡した場合" do
      subject{ params.merged_param(:foo, merge_method: :hoge) }

      before do
        allow(params).to receive(:materialized_param).and_return(foo)
        allow(default).to receive(:foo).and_return(bar)
      end

      context "自身のパラメータ値が渡した名前のメソッドを持つ場合" do
        before do
          allow(foo).to receive(:hoge)
        end

        it{ is_expected.to equal(foo) }
      end

      context "デフォルトのパラメータ値が渡した名前のメソッドを持つ場合" do
        before do
          allow(bar).to receive(:hoge)
        end

        it{ is_expected.to equal(foo) }
      end

      context "両方のパラメータ値が渡した名前のメソッドを持つ場合" do
        before do
          allow(foo).to receive(:hoge)
        end

        it "は、デフォルトと自身のパラメータ値を渡した名前のメソッドでマージした結果を返すこと" do
          expect(bar).to receive(:hoge).with(foo).and_return(baz)
          is_expected.to equal(baz)
        end
      end
    end
  end

  describe "#merge!" do
    subject{ params.merge!(arg) }

    shared_examples_for "return myself" do
      it "は、自身を返すこと" do
        is_expected.to equal(params)
      end
    end

    context "ハッシュを渡した場合" do
      let(:arg){ hash }
      it_behaves_like "return myself"

      it "は、パラメータを一度にセットすること" do
        subject
        expect(params).to have_attributes(
          default: equal(params.default),
          foo: equal(foo),
          bar: equal(_bar),
          baz: equal(baz),
        )
        expect(params.instance_variable_get(:@params)).to match(
          foo: equal(foo),
          baz: equal(baz),
        )
        expect(params.instance_variable_get(:@_params)).to match(
          bar: equal(_bar),
          baz: equal(_baz),
        )
      end
    end

    context "パラメータオブジェクトを渡した場合" do
      let(:arg){ params_class_1.new.merge!(hash) }
      it_behaves_like "return myself"

      it "は、引数が持つパラメータ（`#xxx`の結果のみ）を一度にセットすること" do
        subject
        expect(params).to have_attributes(
          default: equal(default),
          foo: equal(foo),
          bar: equal(_bar),
          baz: equal(baz),
        )
        expect(params.instance_variable_get(:@params)).to match(
          foo: equal(foo),
          baz: equal(baz),
        )
        expect(params.instance_variable_get(:@_params)).to match(
          bar: equal(_bar),
        )
      end
    end
  end

  describe "#merge" do
    subject{ params.merge(arg) }
    let(:arg){ double("Params", to_h: hash) }
    let(:hash){ {bar: "Bar 2", _baz: "_Baz 2"} }

    before do
      params.merge!(foo: "Foo 1", bar: "Bar 1", _baz: "_Baz 1")
    end

    shared_examples_for "return merged params" do
      it "は、自身をデフォルト値に持つパラメータオブジェクトを返すこと" do
        is_expected.to have_attributes(default: equal(params))
      end

      it "は、マージしたパラメータを持つパラメータオブジェクトを返すこと" do
        is_expected.to have_attributes(
          foo: eq("Foo 1"),
          bar: eq("Bar 2"),
          baz: eq("_Baz 2"),
        )
        expect(subject.instance_variable_get(:@params)).to match(bar: "Bar 2")
        expect(subject.instance_variable_get(:@_params)).to match(baz: "_Baz 2")
      end
    end

    context "ハッシュを渡した場合" do
      let(:arg){ hash }

      it_behaves_like "params of the same class as the receiver"
      it_behaves_like "return merged params"
    end

    context "`#to_h`を持つオブジェクトを渡した場合" do
      it_behaves_like "params of the same class as the receiver"
      it_behaves_like "return merged params"
    end

    context "`::class_get`が自身と違うパラメータクラスを返す場合" do
      let(:arg){ hash }

      before do
        allow(params_class_1).to receive(:class_get).and_return(params_class_2)
      end

      it "は、そのクラスのインスタンスを返すこと" do
        is_expected.to be_instance_of(params_class_2)
      end

      it_behaves_like "return merged params"
    end
  end

  describe "#to_h" do
    subject{ params.to_h }

    shared_examples_for "return hash with :foo key" do
      it "は、キー`:foo`にその値を持つハッシュを返すこと" do
        is_expected.to eq({foo: foo, bar: nil, baz: nil})
      end
    end

    shared_examples_for "return hash with :_foo key" do
      it "は、キー`:_foo`にその値を持つハッシュを返すこと" do
        is_expected.to eq({_foo: _foo, bar: nil, baz: nil})
      end
    end

    shared_examples_for "return hash without :foo and :_foo key" do
      it "は、パラメータキーと`nil`のペアのハッシュを返すこと" do
        is_expected.to eq({foo: nil, bar: nil, baz: nil})
      end
    end

    context "パラメータ値`:foo`が`nil`以外の場合" do
      before do
        params.merge!(foo: foo)
      end

      it_behaves_like "return hash with :foo key"
    end

    context "仮パラメータ値`:_foo`が`nil`以外の場合" do
      before do
        params.merge!(_foo: _foo)
      end

      it_behaves_like "return hash with :_foo key"
    end

    context "`#default`が`#to_h`を持つ場合" do
      before do
        allow(default).to receive(:to_h).and_return(hash)
      end

      context "`#default.to_h[:foo]`が`nil`以外を返す場合" do
        let(:hash){ {foo: foo, _foo: _foo} }
        it_behaves_like "return hash with :foo key"
      end

      context "`#default.to_h[:_foo]`が`nil`以外を返す場合" do
        let(:hash){ {foo: nil, _foo: _foo} }
        it_behaves_like "return hash with :_foo key"
      end

      context "どちらのキーも`nil`を返す場合" do
        let(:hash){ {foo: nil, _foo: nil} }
        it_behaves_like "return hash without :foo and :_foo key"
      end

      context "どちらのキーも存在しない場合" do
        let(:hash){ {} }
        it_behaves_like "return hash without :foo and :_foo key"
      end
    end

    context "`#default`が`#foo`を持つ場合" do
      before do
        allow(default).to receive(:foo).and_return(foo)
      end

      it_behaves_like "return hash with :foo key"
    end
  end

  describe "#materialize" do
    subject do
      params.materialize do |key, _value|
        "Materialized #{_value}"
      end
    end

    before do
      params.merge!(_foo: "_foo", bar: "bar", _bar: "_bar", baz: "baz")
    end

    it "は、全てのパラメータを実体化したパラメータオブジェクトを返すこと" do
      is_expected.to be_a(described_class).and have_attributes(
        foo: "Materialized _foo",
        bar: "bar",
        baz: "baz",
      )
    end

    it_behaves_like "params without default"
  end
end
