require 'rails_helper'

describe RouteLink::RouteParams do
  describe "::class_get" do
    subject{ described_class.class_get(arg) }

    shared_examples_for "superclass" do
      it "は、ルーティングパラメータクラスを返すこと" do
        is_expected.to equal(RouteLink::RouteParams)
      end
    end

    shared_examples_for "resourceful" do
      it "は、リソースフル用ルーティングパラメータクラスを返すこと" do
        is_expected.to equal(RouteLink::Resourceful::RouteParams)
      end
    end

    shared_examples_for "uri" do
      it "は、URI用ルーティングパラメータクラスを返すこと" do
        is_expected.to equal(RouteLink::Uri::RouteParams)
      end
    end

    context "キーの配列を渡した場合" do
      context "`:scheme`を含む場合" do
        let(:arg){ [:scheme] }
        it_behaves_like "superclass"
      end

      context "`:controller`を含む場合" do
        let(:arg){ [:controller] }
        it_behaves_like "resourceful"
      end

      context "`:_controller`を含む場合" do
        let(:arg){ [:_controller] }
        it_behaves_like "resourceful"
      end

      context "`:action`を含む場合" do
        let(:arg){ [:action] }
        it_behaves_like "resourceful"
      end

      context "`:path_component`を含む場合" do
        let(:arg){ [:path_component] }
        it_behaves_like "uri"
      end
    end

    context "ハッシュを渡した場合" do
      context "キー`:scheme`を含むハッシュの場合" do
        let(:arg){ {scheme: nil} }
        it_behaves_like "superclass"
      end

      context "キー`:controller`を含むハッシュの場合" do
        let(:arg){ {controller: nil} }
        it_behaves_like "resourceful"
      end

      context "キー`:_controller`を含むハッシュの場合" do
        let(:arg){ {_controller: nil} }
        it_behaves_like "resourceful"
      end

      context "キー`:action`を含むハッシュの場合" do
        let(:arg){ {action: nil} }
        it_behaves_like "resourceful"
      end

      context "キー`:path_component`を含むハッシュの場合" do
        let(:arg){ {path_component: nil} }
        it_behaves_like "uri"
      end
    end

    context "モデルクラスを渡した場合" do
      let(:arg){ Person }
      it_behaves_like "resourceful"
    end

    context "モデルインスタンスを渡した場合" do
      let(:arg){ create(:person) }
      it_behaves_like "resourceful"
    end

    context "シンボルを渡した場合" do
      let(:arg){ :index }
      it_behaves_like "resourceful"
    end

    context "文字列を渡した場合" do
      let(:arg){ "http://foobar/hoge/piyo" }
      it_behaves_like "uri"
    end

    context "URIオブジェクトを渡した場合" do
      let(:arg){ URI.parse("http://foobar/hoge/piyo") }
      it_behaves_like "uri"
    end

    context "それ以外を渡した場合" do
      let(:arg){ 123 }

      it "は、基底ルーティングパラメータクラスを返すこと" do
        is_expected.to equal(RouteLink::RouteParams)
      end
    end
  end

  describe "::query_build" do
    context "第1引数にハッシュを渡した場合" do
      subject{ described_class.query_build(a: "A1", b: "B1") }

      it "は、`#query`が渡したものを返すインスタンスを生成すること" do
        is_expected.to be_instance_of(described_class).and have_attributes(
          query: match(a: "A1", b: "B1"),
        )
      end
    end

    context "オプション`:host`を渡した場合" do
      subject{ described_class.query_build(host: "https") }

      it "は、共通祖先クラスのインスタンスを生成すること" do
        is_expected.to be_instance_of(RouteLink::RouteParams).and have_attributes(
          host: "https",
        )
      end
    end

    context "オプション`:controller`を渡した場合" do
      subject{ described_class.query_build(controller: Person) }

      it "は、リソースフル用インスタンスを生成すること" do
        is_expected.to be_instance_of(RouteLink::Resourceful::RouteParams).and have_attributes(
          controller: Person,
        )
      end
    end

    context "オプション`:_controller`を渡した場合" do
      subject{ described_class.query_build(_controller: Person) }

      it "は、リソースフル用インスタンスを生成すること" do
        is_expected.to be_instance_of(RouteLink::Resourceful::RouteParams).and have_attributes(
          controller: Person,
        )
      end
    end

    context "オプション`:path_component`を渡した場合" do
      subject{ described_class.query_build(path_component: "/hoge/piyo") }

      it "は、非リソースフル用インスタンスを生成すること" do
        is_expected.to be_instance_of(RouteLink::Uri::RouteParams).and have_attributes(
          path_component: "/hoge/piyo",
        )
      end
    end
  end
end
