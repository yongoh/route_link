require 'rails_helper'

describe RouteLink::Resourceful::RouteParams do
  describe "::to_h" do
    subject{ described_class.to_h(arg) }

    context "ハッシュを渡した場合" do
      let(:arg){ {a: "A", b: "B"} }

      it "は、そのまま返すこと" do
        is_expected.to equal(arg).and match(a: "A", b: "B")
      end
    end

    context "配列の配列を渡した場合" do
      let(:arg){ [[:a, "A"], [:b, "B"]] }

      it "は、ハッシュに変換して返すこと" do
        is_expected.to match(a: "A", b: "B")
      end
    end

    context "モデルクラスを渡した場合" do
      let(:arg){ Person }

      it "は、引数をキー`:controller`の値として持つハッシュを返すこと" do
        is_expected.to match(controller: equal(arg))
      end
    end

    context "モデルインスタンスを渡した場合" do
      let(:arg){ create(:person) }

      it "は、引数をキー`:controller`の値として持つハッシュを返すこと" do
        is_expected.to match(controller: equal(arg))
      end
    end

    context "手続きを渡した場合" do
      let(:arg){ ->{} }

      it "は、引数をキー`:_controller`の値として持つハッシュを返すこと" do
        is_expected.to match(_controller: equal(arg))
      end
    end

    context "シンボルを渡した場合" do
      let(:arg){ :index }

      it "は、引数をキー`:action`の値として持つハッシュを返すこと" do
        is_expected.to match(action: equal(arg))
      end
    end

    context "それ以外を渡した場合" do
      let(:arg){ 123 }
      it{ expect{ subject }.to raise_error(NoMethodError, /\Aundefined method `to_h'/) }
    end
  end

  let(:params){ described_class.new.merge!(controller: Person, action: :new) }

  describe "#path" do
    subject{ params.path(view_context) }

    context "パラメータ`#controller`がコントローラ名を返す場合" do
      before do
        allow(params).to receive(:controller).and_return("settings")
      end

      it "は、その名前のコントローラリソースのパスを返すこと" do
        is_expected.to eq("/setting/new")
      end
    end

    context "パラメータ`#controller`がモデルクラスを返す場合" do
      it "は、コレクションリソースのパスを返すこと" do
        is_expected.to eq("/people/new")
      end
    end

    context "パラメータ`#controller`がモデルインスタンスを返す場合" do
      before do
        allow(params).to receive(:controller).and_return(person)
        allow(params).to receive(:action).and_return(:edit)
      end

      let(:person){ create(:person) }

      it "は、メンバーリソースのパスを返すこと" do
        is_expected.to eq("/people/#{person.id}/edit")
      end
    end

    context "パラメータ`#controller`がコントローラ名の配列を返す場合" do
      before do
        allow(params).to receive(:controller).and_return([:foo, :bar, :baz])
      end

      it "は、名前空間付きルーティングのパスを返すこと" do
        is_expected.to eq("/foo/bar/bazs/new")
      end
    end

    context "パラメータ`#controller`が名前空間付きコントローラ名を返す場合" do
      before do
        allow(params).to receive(:controller).and_return("foo/bar/baz")
      end

      it "は、名前空間付きルーティングのパスを返すこと" do
        is_expected.to eq("/foo/bar/bazs/new")
      end
    end

    context "パラメータ`#controller`がレコードとネストされたリソース名の配列を返す場合" do
      before do
        allow(params).to receive(:controller).and_return([person, :profile])
      end

      let(:person){ create(:person) }

      it "は、ネストされたルーティングのパスを返すこと" do
        is_expected.to eq("/people/#{person.id}/profile/new")
      end
    end

    context "パラメータ`#controller`がレコードの配列を返す場合" do
      before do
        allow(params).to receive(:controller).and_return([person, product])
        allow(params).to receive(:action).and_return(:edit)
      end

      let(:person){ create(:person, :with_products) }
      let(:product){ person.products[1] }

      it "は、ネストされたルーティングのパスを返すこと" do
        is_expected.to eq("/people/#{person.id}/products/#{product.id}/edit")
      end
    end

    context "複数対象アクションの場合" do
      before do
        allow(params).to receive(:action).and_return(:search)
      end

      it "は、名前空間付きルーティングのパスを返すこと" do
        is_expected.to eq("/people/search")
      end
    end

    context "パラメータ`#query`がURLクエリハッシュを返す場合" do
      before do
        allow(params).to receive(:query).and_return({a: "A1", b: "B1"})
      end

      it "は、URLクエリを含むパスを返すこと" do
        is_expected.to eq("/people/new?a=A1&b=B1")
      end
    end

    context "パラメータ`#query`がリソースの情報を含むハッシュを返す場合" do
      before do
        allow(params).to receive(:query).and_return({controller: Setting, action: :edit})
      end

      it "は、渡したリソースは無視されること" do
        is_expected.to eq("/people/new")
      end
    end

    context "パラメータ`#query`が`{only_path: false}`を含むハッシュを返す場合" do
      before do
        allow(params).to receive(:query).and_return({only_path: false})
      end

      it "は、渡したオプションは無視されること" do
        is_expected.to eq("/people/new")
      end
    end
  end

  describe "#url" do
    subject{ params.url(view_context) }

    it "は、リソースのフルパスを返すこと" do
      is_expected.to eq("http://www.example.com/people/new")
    end

    context "パラメータ`:host`を設定していた場合" do
      before do
        allow(params).to receive(:host).and_return("foobarbaz.co.jp")
      end

      it "は、設定したホストのフルパスを返すこと" do
        is_expected.to eq("http://foobarbaz.co.jp/people/new")
      end
    end

    context "パラメータ`:scheme`を設定していた場合" do
      before do
        allow(params).to receive(:scheme).and_return("https")
      end

      it "は、設定したスキーマのフルパスを返すこと" do
        is_expected.to eq("https://www.example.com/people/new")
      end
    end

    context "パラメータ`:fragment`を設定していた場合" do
      before do
        allow(params).to receive(:fragment).and_return("hoge")
      end

      it "は、設定したアンカーへのフルパスを返すこと" do
        is_expected.to eq("http://www.example.com/people/new#hoge")
      end
    end

    context "パラメータ`:userinfo`を設定していた場合" do
      before do
        allow(params).to receive(:userinfo).and_return("user:pass")
      end

      it "は、設定したユーザー情報のフルパスを返すこと" do
        is_expected.to eq("http://user:pass@www.example.com/people/new")
      end
    end
  end

  describe "#human_controller_name" do
    subject{ params.human_controller_name }

    context "キー'routes/controllers/{controller}\#'の訳文が存在する場合" do
      let(:params){ described_class.new.merge!(controller: "settings") }

      it "は、コントローラ名の訳文を返すこと" do
        is_expected.to eq("設定")
      end
    end

    context "コントローラ名の訳文が存在せず、モデル名の訳文が存在する場合" do
      let(:params){ described_class.new.merge!(controller: "people") }

      it "は、モデル名の訳文を返すこと" do
        is_expected.to eq("人物")
      end
    end

    context "コントローラ名もモデル名も訳文が存在しない場合" do
      let(:params){ described_class.new.merge!(controller: "missing_controller") }

      it "は、英文化したコントローラ名を返すこと" do
        is_expected.to eq("Missing controller")
      end
    end

    context "名前空間付きコントローラ名の訳文が存在する場合" do
      let(:params){ described_class.new.merge!(controller: [:foo, :bar, :baz]) }

      it "は、コントローラ名の訳文を返すこと" do
        is_expected.to eq("フーバーバズ")
      end
    end

    context "名前空間付きコントローラ名の訳文が存在しない場合" do
      let(:params){ described_class.new.merge!(controller: [:foo, :bar, :baz, :qux]) }

      it "は、英文化したコントローラ名を返すこと" do
        is_expected.to eq("Foo/bar/baz/qux")
      end
    end
  end

  describe "#human_action_name" do
    subject{ params.human_action_name }

    context "キー'routes/actions/{controller}\#{action}'の訳文が存在する場合" do
      let(:params){ described_class.new.merge!(controller: Person, action: :hoge) }

      it "は、アクション名のコントローラ特有の訳文を返すこと" do
        is_expected.to eq("ほげ")
      end
    end

    context "キー'routes/actions/{action}'の訳文が存在する場合" do
      let(:params){ described_class.new.merge!(controller: Person, action: :show) }

      it "は、アクション名のデフォルトの訳文を返すこと" do
        is_expected.to eq("詳細")
      end
    end

    context "どちらも存在する場合" do
      let(:params){ described_class.new.merge!(controller: Person, action: :search) }

      it "は、アクション名のコントローラ特有の訳文を返すこと" do
        is_expected.to eq("けんさく")
      end
    end

    context "どちらも存在しない場合" do
      let(:params){ described_class.new.merge!(controller: Person, action: :missing_action) }

      it "は、英文化したアクション名を返すこと" do
        is_expected.to eq("Missing action")
      end
    end

    context "名前空間付きルーティングの場合" do
      let(:params){ described_class.new.merge!(controller: [:foo, :bar, :baz], action: :hoge) }

      it "は、アクション名のコントローラ特有の訳文を返すこと" do
        is_expected.to eq("ホゲ")
      end
    end
  end
end
