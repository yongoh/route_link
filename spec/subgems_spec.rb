Dir.glob("route_link-*") do |root_path|
  require_relative "../#{root_path}/spec/rails_helper"

  Dir.glob("#{root_path}/spec/**/*_spec.rb") do |spec_path|
    require_relative "../#{spec_path}"
  end
end
