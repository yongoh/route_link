class AddPersonIdToProduct < ActiveRecord::Migration[5.0]
  def change
    change_table :products do |t|
      t.references :person, foreign_key: true
    end
  end
end
