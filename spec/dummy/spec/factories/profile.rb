FactoryBot.define do
  factory :profile do
    sequence(:name){|n| "NAME#{n}" }
  end
end
