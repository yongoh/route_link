FactoryBot.define do
  factory :person do
    sequence(:name){|n| "NAME#{n}" }

    trait :with_products do
      products{ create_list(:product, 3) }
    end
  end
end
