Rails.application.routes.draw do
  resource :setting
  resources :people do
    resource :profile
    resources :products
  end
  namespace :foo do
    namespace :bar do
      resources :bazs
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
