# RouteLink
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'route_link'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install route_link
```

## 使用方法
### ビューヘルパー
```erb
<%= route_link_to({controller: Person, action: :new}, {id: "my-id", class: "my-class"}) %>
```
```html
<a class="my-class" href="/people/new" id="my-id">人物の新規作成</a>
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
