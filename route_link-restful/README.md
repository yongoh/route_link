# RouteLink
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'route_link-restful'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install route_link-restful
```

## 使用方法
```ruby
class Decorator
  include RouteLink::Restful

  attr_reader :object

  def initialize(object)
    @object = object
  end
end
```
### 複数形リソースへのリンクメソッドを定義
```ruby
class PersonDecorator < Decorator
  routes resources: true
end
```
```ruby
decorator = PersonDecorator.new(Person.create)
decorator.index_link   #=> <a href="/person">一覧</a>
decorator.new_link     #=> <a href="/person/new">新規作成</a>
decorator.show_link    #=> <a href="/person/1">詳細</a>
decorator.edit_link    #=> <a href="/person/1/edit">編集</a>
decorator.destroy_link #=> <a href="/person/1" data-method="delete" data-confirm="Are you sure?">削除</a>
decorator.search_link  #=> <a href="/person/search">検索</a>
```
### 単数形リソースへのリンクメソッドを定義
```ruby
class SettingDecorator < Decorator
  routes resource: true
end
```
```ruby
decorator = SettingDecorator.new(Setting.create)
decorator.new_link     #=> <a href="/setting/new">新規作成</a>
decorator.show_link    #=> <a href="/setting">詳細</a>
decorator.edit_link    #=> <a href="/setting/edit">編集</a>
decorator.destroy_link #=> <a href="/setting" data-method="delete" data-confirm="Are you sure?">削除</a>
```
### コレクションルーティングへのリンクメソッドを定義
```ruby
class PersonDecorator < Decorator
  routes do
    collection :foo
  end
end
```
```ruby
decorator = PersonDecorator.new(Person.create)
decorator.foo_link #=> <a href="/person/foo">Foo</a>
```
### メンバールーティングへのリンクメソッドを定義
```ruby
class PersonDecorator < Decorator
  routes do
    member :bar
  end
end
```
```ruby
decorator = PersonDecorator.new(Person.create)
decorator.bar_link #=> <a href="/person/1/bar">Bar</a>
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
