require 'route_link/restful/railtie'

module RouteLink
  module Restful
    extend ActiveSupport::Concern

    included do
      extend Decoratable

      routes do |config|

        # ラッパーメソッドを定義
        config.wrap :collection do |route|
          route.route_params(_controller: proc{|lb, d| d.object.model_name.route_key }, action: route.name)
          route.link_params(format: :action)
        end

        config.wrap :member do |route|
          route.route_params(_controller: proc{|lb, d| d.object }, action: route.name)
          route.link_params(format: :action)
        end

        # 複数形リソースのルーティンググループを定義
        config.route_group :resources do
          collection :index
          member :show
          member :edit
          collection :new
          member :destroy, {}, _html_attributes: :destroy_link_html_attributes
          collection :search
        end

        # 単数形リソースのルーティンググループを定義
        config.route_group :resource do
          collection :new
          collection :show
          collection :edit
          collection :destroy, {}, _html_attributes: :destroy_link_html_attributes
        end
      end
    end

    class << self

      # @param [Symbol,String] action アクション名
      # @param [Hash] i18n_params 訳文パラメータ
      # @return [String] 確認ダイアログの文章
      def confirm_text(action, **i18n_params)
        I18n.t(action, scope: ROUTE_NAME_SCOPE + [:confirm], **i18n_params)
      end
    end

    # @return [Hash] アクション`destroy`へのリンク要素のHTML属性
    def destroy_link_html_attributes(link_builder)
      {method: "delete", data: {confirm: Restful.confirm_text(:destroy)}}
    end
  end
end
