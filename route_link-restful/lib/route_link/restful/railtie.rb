module RouteLink
  module Restful
    class Railtie < ::Rails::Railtie

      initializer "Patch routing" do
        ActionDispatch::Routing::Mapper::Resource.prepend(Module.new do
          def default_actions
            if @api_only
              super
            else
              super + [:search]
            end
          end
        end)

        ActionDispatch::Routing::Mapper.include(Module.new do
          def resources(*args)
            super(*args) do
              yield if block_given?
              collection{ get :search } if parent_resource.actions.include?(:search)
            end
          end
        end)
      end
    end
  end
end
