require 'rails_helper'

describe RouteLink::Restful do
  let(:decorator_class){ Struct.new(:object, :helpers).include(described_class) }
  let(:decorator){ decorator_class.new(person, view_context) }
  let(:person){ create(:person) }

  describe "#routes" do
    subject do
      decorator_class.instance_methods
    end

    context "引数・ブロックなしの場合" do
      before do
        decorator_class.send(:routes)
      end

      it "は、一切のルーティングメソッドを定義しないこと" do
        is_expected.not_to be_any{|method_name| /_(path|url|name|link)$/.match(method_name) }
      end
    end

    context "オプション`:resources`に`true`を渡した場合" do
      before do
        decorator_class.send(:routes, resources: true)
      end

      it "は、デコレータクラスに複数形リソースのルーティングメソッド群を定義すること" do
        is_expected.to include(
          :index_path, :index_url, :index_name, :index_link,
          :new_path, :new_url, :new_name, :new_link,
          :show_path, :show_url, :show_name, :show_link,
          :edit_path, :edit_url, :edit_name, :edit_link,
          :destroy_path, :destroy_url, :destroy_name, :destroy_link,
        )

        expect(decorator.index_path).to eq("/people")
        expect(decorator.new_path).to eq("/people/new")
        expect(decorator.show_path).to eq("/people/#{person.id}")
        expect(decorator.edit_path).to eq("/people/#{person.id}/edit")
        expect(decorator.destroy_path).to eq("/people/#{person.id}")
        expect(decorator.destroy_link).to have_tag(
          "a",
          with: {
            href: "/people/#{person.id}",
            "data-method" => "delete",
            "data-confirm" => "削除します。よろしいですか？",
          },
          text: "削除",
        )
      end
    end

    context "オプション`:resource`に`true`を渡した場合" do
      before do
        decorator_class.send(:routes, resource: true)
      end

      let(:decorator){ decorator_class.new(create(:setting), view_context) }

      it "は、デコレータクラスに単数形リソースのルーティングメソッド群を定義すること" do
        is_expected.to include(
          :new_path, :new_url, :new_name, :new_link,
          :show_path, :show_url, :show_name, :show_link,
          :edit_path, :edit_url, :edit_name, :edit_link,
          :destroy_path, :destroy_url, :destroy_name, :destroy_link,
        )
        is_expected.to_not include(:index_path, :index_url, :index_link,)

        expect(decorator.new_path).to eq("/setting/new")
        expect(decorator.show_path).to eq("/setting")
        expect(decorator.edit_path).to eq("/setting/edit")
        expect(decorator.destroy_path).to eq("/setting")
        expect(decorator.destroy_link).to have_tag(
          "a",
          with: {
            href: "/setting",
            "data-method" => "delete",
            "data-confirm" => "削除します。よろしいですか？",
          },
          text: "削除",
        )
      end
    end

    context "ブロック内でルーティング生成メソッドを使った場合" do
      before do
        decorator_class.send(:routes) do
          path :new, controller: Person, action: :new
        end
      end

      it "は、デコレータクラスにルーティングメソッドを定義すること" do
        expect(decorator.new_path).to eq("/people/new")
      end
    end

    context "ブロック内で`#collection`を使った場合" do
      before do
        decorator_class.send(:routes) do
          collection :new
        end
      end

      it "は、デコレータクラスにコレクションルートへのルーティングメソッドを定義すること" do
        expect(decorator.new_name).to eq("新規作成")
        expect(decorator.new_path).to eq("/people/new")
        expect(decorator.new_url).to eq("http://www.example.com/people/new")
        expect(decorator.new_link).to have_tag("a", with: {href: "/people/new"}, text: "新規作成")
      end
    end

    context "ブロック内で`#member`を使った場合" do
      before do
        decorator_class.send(:routes) do
          member :edit
        end
      end

      it "は、デコレータクラスにメンバールートへのルーティングメソッドを定義すること" do
        expect(decorator.edit_name).to eq("編集")
        expect(decorator.edit_path).to eq("/people/#{person.id}/edit")
        expect(decorator.edit_url).to eq("http://www.example.com/people/#{person.id}/edit")
        expect(decorator.edit_link).to have_tag("a", with: {href: "/people/#{person.id}/edit"}, text: "編集")
      end
    end
  end
end
