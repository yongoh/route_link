$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "route_link/decoratable/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "route_link-decoratable"
  spec.version     = RouteLink::Decoratable::VERSION
  spec.authors     = ["yongoh"]
  spec.email       = ["a.yongoh@gmail.com"]
  spec.homepage    = ""
  spec.summary     = "Define route link method for decorator."
  spec.description = ""
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "route_link"

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency "rspec-html-matchers"
  spec.add_development_dependency "database_cleaner"
  spec.add_development_dependency "factory_bot_rails"
end
