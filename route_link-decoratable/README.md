# RouteLink
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'route_link-decoratable'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install route_link-decoratable
```

## 使用方法
### リンクメソッドの定義と使用方法
#### リソースフルなリンクメソッドを定義
```ruby
class Decorator
  extend RouteLink::Decoratable

  routes do
    route :foo, controller: Person, action: :new
  end
end
```
```ruby
decorator = Decorator.new
decorator.foo_link #=> <a href="/person/new">新規作成</a>
```
#### URLを渡してリンクメソッドを定義
```ruby
class Decorator
  extend RouteLink::Decoratable

  routes do
    route :bar, "http://www.example.com/people/1"
  end
end
```
```ruby
decorator = Decorator.new
decorator.bar_link #=> <a href="/people/1">/people/1</a>
```

#### URI構成要素を渡してリンクメソッドを定義
```ruby
class Decorator
  extend RouteLink::Decoratable

  routes do
    route :baz, host: "foobarbaz.co.jp", path_component: "/hoge/piyo"
  end
end
```
```ruby
decorator = Decorator.new
decorator.baz_link #=> <a href="http://foobarbaz.co.jp/hoge/piyo">http://foobarbaz.co.jp/hoge/piyo</a>
```

### ラッパーメソッドの定義と使用方法
```ruby
class Decorator
  extend RouteLink::Decoratable

  routes do |config|
    config.wrap :hoge, controller: Person
  end
end
```
```ruby
class SubDecorator < Decorator
  routes do
    hoge :foo, action: :new
    hoge :baz, action: :index
  end
end
```
```ruby
decorator = SubDecorator.new
decorator.foo_link #=> <a href="/person/new">新規作成</a>
decorator.bar_link #=> <a href="/person">一覧</a>
```
### ルーティンググループの定義と使用方法
```ruby
class Decorator
  extend RouteLink::Decoratable

  routes do |config|
    config.route_group :foobar do
      route :foo, controller: Person, action: :new
      route :bar, "http://www.example.com/people/1"
    end
  end
end
```
```ruby
class SubDecorator < Decorator
  routes foobar: true
end
```
もしくは
```ruby
class SubDecorator < Decorator
  routes do
    foobar
  end
end
```
```ruby
decorator = SubDecorator.new
decorator.foo_link #=> <a href="/person/new">新規作成</a>
decorator.bar_link #=> <a href="/people/1">/people/1</a>
```

### デフォルトパラメータの設定
```ruby
class Decorator
  extend RouteLink::Decoratable

  routes do |config|
    config.route_params(query: {a: "A1", b: "B1"})
    config.link_params(
      _format: :action_with_object,
      i18n_params: {Object: "ふー"},
      html_attributes: {id: "my-id", class: "my-class"},
    )

    route :foo, controller: Person, action: :new
  end
end
```
```ruby
decorator = SubDecorator.new
decorator.foo_link #=> <a href="/person/new?a=A1&b=B1" id="my-id" class="my-class">ふーの新規作成</a>
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
