require 'rails_helper'

describe "Inheritance" do
  let(:decorator_class){
    Struct.new(:helpers) do
      extend RouteLink::Decoratable

      routes do |config|
        config.route_params(query: {a: "A", b: "B", c: "C"})

        config.wrap :wrap1 do |c|
          c.link_params(html_attributes: {a: "HA", b: "HB", c: "HC"})
        end

        config.route_group :group1 do
          route :index, controller: Person
          route :new, controller: Person
        end
      end
    end
  }
  let(:x_class){
    Class.new(decorator_class) do
      routes do |config|
        config.route_params(query: {b: "Bx"})

        config.wrap :wrap1 do |c|
          c.link_params(html_attributes: {b: "HBx"})
        end

        wrap1 do
          route :hoge, "/hoge", {html_attributes: {c: "HCx"}}
        end

        group1
      end
    end
  }
  let(:xx_class){
    Class.new(x_class) do
      routes do |config|
        config.route_params(query: {c: "Cxx"})

        wrap1 do
          route :hoge, "/hoge", {html_attributes: {c: "HCxx"}}
        end
      end
    end
  }
  let(:y_class){
    Class.new(decorator_class) do
      routes do |config|
        config.route_params(query: {c: "Cy"})

        wrap1 do
          route :hoge, "/hoge", {html_attributes: {a: "HAy"}}
        end

        group1 only: [:index]
      end
    end
  }
  let(:z_class){
    Class.new(decorator_class) do
      routes group1: {only: [:index]}
    end
  }

  let(:x){ x_class.new(view_context) }
  let(:xx){ xx_class.new(view_context) }
  let(:y){ y_class.new(view_context) }
  let(:z){ z_class.new(view_context) }

  example "それぞれのクラスは別個の設定オブジェクトを持っていること" do
    expect(x_class.send(:routes){|c| break c }).not_to equal(y_class.send(:routes){|c| break c })
    expect(x_class.send(:routes){|c| break c }).not_to equal(z_class.send(:routes){|c| break c })
    expect(y_class.send(:routes){|c| break c }).not_to equal(z_class.send(:routes){|c| break c })

    expect(x_class.send(:routes){|c| break c }).not_to equal(decorator_class.send(:routes){|c| break c })
    expect(y_class.send(:routes){|c| break c }).not_to equal(decorator_class.send(:routes){|c| break c })
    expect(z_class.send(:routes){|c| break c }).not_to equal(decorator_class.send(:routes){|c| break c })
  end

  example "直系祖先のパラメータのみマージすること" do
    expect(x.index_path).to eq("/people?a=A&b=Bx&c=C")
    expect(xx.index_path).to eq("/people?a=A&b=Bx&c=C")
    expect(y.index_path).to eq("/people?a=A&b=B&c=Cy")
    expect(z.index_path).to eq("/people?a=A&b=B&c=C")
  end

  example "ルーティンググループメソッドへ渡された引数により一部のルーティングメソッドだけ定義されていること" do
    expect(y).to be_respond_to(:index_path)
    expect(y).not_to be_respond_to(:new_path)
  end

  example "`#routes`へ渡された引数により一部のルーティングメソッドだけ定義されていること" do
    expect(z).to be_respond_to(:index_path)
    expect(z).not_to be_respond_to(:new_path)
  end

  example "直系祖先のラッパー群のみマージすること" do
    expect(x.hoge_link).to have_tag("a", with: {a: "HA", b: "HBx", c: "HCx"})
    expect(xx.hoge_link).to have_tag("a", with: {a: "HA", b: "HBx", c: "HCxx"})
    expect(y.hoge_link).to have_tag("a", with: {a: "HAy", b: "HB", c: "HC"})
  end
end
