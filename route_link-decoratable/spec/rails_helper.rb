spec_dir = File.expand_path('../../spec', __dir__)
$LOAD_PATH << spec_dir
require File.join(spec_dir, "rails_helper")

unless FactoryBot.definition_file_paths.any?{|path| Dir.exist?(path) }
  FactoryBot.definition_file_paths << File.join(spec_dir, "factories")
  FactoryBot.find_definitions
end
