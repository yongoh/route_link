require 'rails_helper'

describe RouteLink::Decoratable do
  let(:decorator_class){ Struct.new(:helpers).extend(described_class) }
  let(:decorator){ decorator_class.new(view_context) }

  describe "#routes" do
    describe "ブロック引数" do
      before do
        decorator_class.send(:routes) do |config|
          config.link_params.format = "C1 Format"
          config.method_params.i18n_params = {foo: "C1 Foo"}
        end
        sub_class.send(:routes) do |config|
          config.link_params.format = nil
          config.method_params.i18n_params = {bar: "C2 Bar", baz: "C2 Baz"}
        end
        subsub_class.send(:routes) do |config|
          config.link_params.format = "C3 Format"
          config.method_params.i18n_params = {baz: "C3 Baz", qux: "C3 Qux"}
        end
        mod1.send(:routes) do |config|
          config.link_params.format = "M1 Format"
          config.method_params.i18n_params = {foo: "M1 Foo"}
        end
        mod2.send(:routes) do |config|
          config.link_params.format = "M2 Format"
          config.method_params.i18n_params = {bar: "M2 Bar", baz: "M3 Baz"}
        end
      end

      let(:sub_class){ Class.new(decorator_class).include(mod2) }
      let(:subsub_class){ Class.new(sub_class) }
      let(:mod1){ Module.new.extend(described_class) }
      let(:mod2){ Module.new.extend(described_class).include(mod1) }

      it "は、設定オブジェクトを渡すこと" do
        expect{|block| decorator_class.send(:routes, &block) }.to yield_with_args(be_a(RouteLink::Config))
      end

      it "は、メモ化すること" do
        expect(decorator_class.send(:routes){|c| break c }).to equal(decorator_class.send(:routes){|c| break c })
      end

      it "が返すパラメータオブジェクトは、親クラスのパラメータをデフォルト値に持つこと" do
        r = self
        decorator_class.send(:routes) do |config|
          r.expect(config.link_params.format).to r.eq("C1 Format")
          r.expect(config.method_params.i18n_params).to r.match(foo: "C1 Foo")
        end
        sub_class.send(:routes) do |config|
          r.expect(config.link_params.format).to r.eq("M2 Format")
          r.expect(config.method_params.i18n_params).to r.match(foo: "M1 Foo", bar: "C2 Bar", baz: "C2 Baz")
        end
        subsub_class.send(:routes) do |config|
          r.expect(config.link_params.format).to r.eq("C3 Format")
          r.expect(config.method_params.i18n_params).to r.match(foo: "M1 Foo", bar: "C2 Bar", baz: "C3 Baz", qux: "C3 Qux")
        end
      end
    end

    context "ブロック内でルーティングメソッド定義メソッドを使った場合" do
      context "リソースフルなルーティングの場合" do
        before do
          decorator_class.send(:routes) do |config|
            route :foo, controller: Person, action: :index
          end
        end

        it "は、ルーティングメソッド群をデコレータクラスに生成すること" do
          expect(decorator.foo_name).to eq("/people")
          expect(decorator.foo_path).to eq("/people")
          expect(decorator.foo_url).to eq("http://www.example.com/people")
          expect(decorator.foo_link).to have_tag("a", with: {href: "/people"}, text: "/people")
        end
      end

      context "非リソースフルなルーティングの場合" do
        before do
          decorator_class.send(:routes) do |config|
            route :bar, "/hoge/piyo"
          end
        end

        it "は、ルーティングメソッド群をデコレータクラスに生成すること" do
          expect(decorator.bar_name).to eq("/hoge/piyo")
          expect(decorator.bar_path).to eq("/hoge/piyo")
          expect(decorator.bar_url).to eq("//www.example.com/hoge/piyo")
          expect(decorator.bar_link).to have_tag("a", with: {href: "/hoge/piyo"}, text: "/hoge/piyo")
        end
      end
    end

    context "ブロック内でラッパーメソッドを使った場合" do
      before do
        decorator_class.send(:routes) do |config|
          config.wrap :foo do |config_or_route|
            config_or_route.route_params(controller: Person)
          end
        end
      end

      context "ブロック無しでパラメータを渡した場合" do
        before do
          decorator_class.send(:routes) do
            foo :new, action: :new
          end
        end

        it "は、全種類のルーティングメソッドをデコレータクラスに定義すること" do
          expect(decorator.new_name).to eq("/people/new")
          expect(decorator.new_path).to eq("/people/new")
          expect(decorator.new_url).to eq("http://www.example.com/people/new")
          expect(decorator.new_link).to have_tag("a", with: {href: "/people/new"}, text: "/people/new")
        end
      end

      context "ブロック内でルーティング生成メソッドを使った場合" do
        before do
          decorator_class.send(:routes) do
            foo do
              path :new, action: :new
            end
          end
        end

        it "は、設定したルーティングメソッドだけをデコレータクラスに定義すること" do
          expect(decorator.new_path).to eq("/people/new")
          expect(decorator.methods).not_to include(:new_name, :new_url, :new_link)
        end
      end
    end

    def set_route_groups
      decorator_class.send(:routes) do |config|
        config.route_group :foo do
          name :show, controller: Setting, action: :show
          path :edit, controller: Setting, action: :edit
          route :new, controller: Setting, action: :new
        end
      end
    end

    shared_examples_for "extract all" do
      it "は、キーの名前のルーティンググループが持つルーティングメソッド全てをデコレータクラスに生成すること" do
        expect(decorator.show_name).to eq("/setting")
        expect(decorator.edit_path).to eq("/setting/edit")
        expect(decorator.new_name).to eq("/setting/new")
        expect(decorator.new_path).to eq("/setting/new")
        expect(decorator.new_url).to eq("http://www.example.com/setting/new")
        expect(decorator.new_link).to have_tag("a", with: {href: "/setting/new"}, text: "/setting/new")
      end
    end

    shared_examples_for "extract only" do
      it "は、キーの名前のルーティンググループが持つうちキー`:only`に渡した名前のルーティングメソッドだけをデコレータクラスに生成すること" do
        expect(decorator.show_name).to eq("/setting")
        expect(decorator.edit_path).to eq("/setting/edit")
        expect(decorator.public_methods).not_to include(:new_name, :new_path, :new_url, :new_link)
      end
    end

    shared_examples_for "extract except" do
      it "は、キーの名前のルーティンググループが持つうちキー`:except`に渡した名前のルーティングメソッド以外をデコレータクラスに生成すること" do
        expect(decorator.show_name).to eq("/setting")
        expect(decorator.public_methods).not_to include(:edit_path, :new_name, :new_path, :new_url, :new_link)
      end
    end

    context "ブロック内でルーティンググループメソッドを使った場合" do
      before do
        set_route_groups
      end

      context "引数無しの場合" do
        before do
          decorator_class.send(:routes) do
            foo
          end
        end

        it_behaves_like "extract all"
      end

      context "オプション`:only`にアクション名の配列を渡した場合" do
        before do
          decorator_class.send(:routes) do
            foo only: [:show, :edit]
          end
        end

        it_behaves_like "extract only"
      end

      context "オプション`:except`にアクション名の配列を渡した場合" do
        before do
          decorator_class.send(:routes) do
            foo except: [:edit, :new]
          end
        end

        it_behaves_like "extract except"
      end
    end

    context "ルーティンググループ名をキーとしたハッシュを渡した場合" do
      before do
        set_route_groups
      end

      context "値に`true`を渡した場合" do
        before do
          decorator_class.send(:routes, foo: true)
        end

        it_behaves_like "extract all"
      end

      context "値にキー`:only`を含むハッシュを渡した場合" do
        before do
          decorator_class.send(:routes, foo: {only: [:show, :edit]})
        end

        it_behaves_like "extract only"
      end

      context "値にキー`:except`を含むハッシュを渡した場合" do
        before do
          decorator_class.send(:routes, foo: {except: [:edit, :new]})
        end

        it_behaves_like "extract except"
      end
    end
  end
end
