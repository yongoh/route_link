require 'rails_helper'

describe RouteLink::Decoratable::RouteGroup do
  let(:route_group){ described_class.new(config) }
  let(:owner){ double("Decorator", helpers: view_context).extend(route_group.mod) }
  let(:config){ double("Config") }

  before do
    route_group << foo
    route_group << bar
    route_group << baz
  end

  let(:foo){ double("Route", name: :foo) }
  let(:bar){ double("Route", name: :bar) }
  let(:baz){ double("Route", name: :baz) }

  describe "#[]" do
    it "は、渡した名前のルーティングを返すこと" do
      expect(route_group[:bar]).to equal(bar)
    end
  end

  describe "#extract" do
    context "オプション無しの場合" do
      subject do
        route_group.extract
      end

      it "は、自身と同じルーティング群を持つルーティンググループを返すこと" do
        is_expected.not_to equal(described_class)
        is_expected.to be_instance_of(described_class).and contain_exactly(equal(foo), equal(bar), equal(baz))
      end
    end

    context "オプション`:only`にルーティング名の配列を渡した場合" do
      subject do
        route_group.extract(only: [:foo])
      end

      it "は、指定した名前のルーティングを抽出したルーティンググループを返すこと" do
        is_expected.not_to equal(described_class)
        is_expected.to be_instance_of(described_class).and contain_exactly(equal(foo))
      end
    end

    context "オプション`:except`にルーティング名の配列を渡した場合" do
      subject do
        route_group.extract(except: [:foo])
      end

      it "は、指定した名前のルーティングを除外したルーティンググループを返すこと" do
        is_expected.not_to equal(described_class)
        is_expected.to be_instance_of(described_class).and contain_exactly(equal(bar), equal(baz))
      end
    end

    context "オプション`:only`と`:except`を同時に渡した場合" do
      subject do
        route_group.extract(only: [:foo, :bar], except: [:bar])
      end

      it "は、自身と同じルーティング群を持つルーティンググループを返すこと" do
        is_expected.not_to equal(described_class)
        is_expected.to be_instance_of(described_class).and contain_exactly(equal(foo))
      end
    end
  end

  describe "#routes" do
    let(:config){ RouteLink::Config.new }

    it "は、自身を返すこと" do
      expect(route_group.routes).to equal(route_group)
    end

    it "は、ブロック引数に自身が持つ設定オブジェクトを渡すこと" do
      expect{|block| route_group.routes(&block) }.to yield_with_args(equal(config))
    end

    it "のブロック内selfは自身を持つメソッドビルダーオブジェクトであること" do
      yield_self = route_group.routes{ break self }
      expect(yield_self).to be_a(RouteLink::Decoratable::MethodBuilder)
      expect(yield_self.instance_variable_get(:@route_group)).to equal(route_group)
    end
  end
end
