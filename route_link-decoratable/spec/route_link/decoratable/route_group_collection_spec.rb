require 'rails_helper'

describe RouteLink::Decoratable::RouteGroupCollection do
  let(:collection){ described_class.new(config) }
  let(:config){ RouteLink::Config.new }

  def set_route_groups
    %i(foo bar baz).each do |name|
      collection.define(name) do
        link :index,  {controller: Person}, {format: name}
        link :new,    {controller: Person}, {format: name}
        link :search, {controller: Person}, {format: name}
      end
    end
  end

  describe "#[]" do
    context "存在しないキーを渡した場合" do
      it "は、空のルーティンググループを返すこと" do
        expect(collection[:hoge]).to be_a(RouteLink::Decoratable::RouteGroup).and be_empty
      end

      it "は、そのキーに戻り値を格納すること" do
        expect(collection[:hoge]).to equal(collection[:hoge])
      end
    end
  end

  describe "#define" do
    it "は、ブロック引数に　設定オブジェクトを渡すこと" do
      expect{|block| collection.define(:hoge, &block) }.to yield_with_args(be_instance_of(RouteLink::Config))
    end

    it "のブロック内selfはメソッドビルダーであること" do
      expect(collection.define(:hoge){ break self }).to be_a(RouteLink::Decoratable::MethodBuilder)
    end

    it "は、第1引数に渡した名前のルーティンググループを返すこと" do
      expect(collection.define(:hoge){}).to equal(collection.fetch(:hoge))
    end

    it "は、第1引数に渡した名前と同じ名前のメソッドを`@mod`に定義すること" do
      set_route_groups
      expect(collection.mod).to be_a(Module).and have_attributes(
        instance_methods: contain_exactly(:foo, :bar, :baz),
      )
    end

    describe "生成されたメソッド" do
      let(:route_group){ RouteLink::Decoratable::RouteGroup.new(config) }
      let(:builder){ config.method_builder_build(route_group).extend(collection.mod) }

      before do
        set_route_groups
        builder.link :show, {controller: Person}, {format: :foobar}
        builder.link :edit, {controller: Person}, {format: :foobar}
      end

      context "オプション無しの場合" do
        subject do
          builder.foo
        end

        it "は、メソッド名と同じ名前のルーティンググループが持つ全てのルーティングを追加すること" do
          subject
          expect(route_group).to match([
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :show,
              link_params: have_attributes(format: :foobar),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :edit,
              link_params: have_attributes(format: :foobar),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :index,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :new,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :search,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
          ])
        end

        it "は、抽出したルーティングの配列を返すこと" do
          is_expected.to match([
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :index,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :new,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :search,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
          ])
        end
      end

      context "オプション`:only`にルーティング名の配列を渡した場合" do
        subject do
          builder.foo(only: [:index, :new])
        end

        it "は、メソッド名と同じ名前のルーティンググループが持つうち渡した名前のルーティングを追加すること" do
          subject
          expect(route_group).to match([
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :show,
              link_params: have_attributes(format: :foobar),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :edit,
              link_params: have_attributes(format: :foobar),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :index,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :new,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
          ])
        end

        it "は、抽出したルーティングの配列を返すこと" do
          is_expected.to match([
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :index,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :new,
              link_params: have_attributes(format: :foo),
              method_params: have_attributes(types: [:link]),
            )),
          ])
        end
      end
    end
  end

  describe "#extract" do
    before do
      set_route_groups
    end

    context "引数なしの場合" do
      it "は、空のルーティンググループを返すこと" do
        expect(collection.extract).to be_a(RouteLink::Decoratable::RouteGroup).and be_empty
      end
    end

    context "ルーティンググループ名とハッシュのペアのハッシュを渡した場合" do
      subject do
        collection.extract(foo: {only: [:index, :new]}, bar: {except: [:new]})
      end

      it "は、抽出したルーティングメソッドオブジェクトを持つルーティンググループを返すこと" do
        is_expected.to be_a(RouteLink::Decoratable::RouteGroup).and match([
          be_a(RouteLink::Decoratable::Route).and(have_attributes(
            name: :index,
            link_params: have_attributes(format: :bar),
            method_params: have_attributes(types: [:link]),
          )),
          be_a(RouteLink::Decoratable::Route).and(have_attributes(
            name: :new,
            link_params: have_attributes(format: :foo),
            method_params: have_attributes(types: [:link]),
          )),
          be_a(RouteLink::Decoratable::Route).and(have_attributes(
            name: :search,
            link_params: have_attributes(format: :bar),
            method_params: have_attributes(types: [:link]),
          )),
        ])
      end
    end

    context "ルーティンググループ名と真偽値のペアのハッシュを渡した場合" do
      subject do
        collection.extract(foo: true, bar: false, baz: true)
      end

      it "は、`true`を渡したグループが持つ全てのルーティングメソッドオブジェクトを持つルーティンググループを返すこと" do
        is_expected.to be_a(RouteLink::Decoratable::RouteGroup).and match([
          be_a(RouteLink::Decoratable::Route).and(have_attributes(
            name: :index,
            link_params: have_attributes(format: :baz),
            method_params: have_attributes(types: [:link]),
          )),
          be_a(RouteLink::Decoratable::Route).and(have_attributes(
            name: :new,
            link_params: have_attributes(format: :baz),
            method_params: have_attributes(types: [:link]),
          )),
          be_a(RouteLink::Decoratable::Route).and(have_attributes(
            name: :search,
            link_params: have_attributes(format: :baz),
            method_params: have_attributes(types: [:link]),
          )),
        ])
      end
    end
  end
end
