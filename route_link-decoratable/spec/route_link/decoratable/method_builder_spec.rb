require 'rails_helper'

describe RouteLink::Decoratable::MethodBuilder do
  let(:builder){ described_class.new(route_group) }
  let(:route_group){ RouteLink::Decoratable::RouteGroup.new(config) }
  let(:config){ Rails.configuration.x.route_link }

  RouteLink::Decoratable::MethodBuilder::METHOD_TYPES.each do |method_type|
    describe "##{method_type}" do
      it "は、ルーティングメソッドオブジェクトをルーティンググループに追加すること" do
        expect{ builder.public_send(method_type, :index) }.to change{ route_group.size }.by(1)
        expect(route_group).to include(
          be_instance_of(RouteLink::Decoratable::Route).and have_attributes(
            name: :index,
            method_params: have_attributes(types: [method_type]),
          )
        )
      end
    end
  end

  describe "#route" do
    it "は、ルーティングメソッドオブジェクトをルーティンググループに追加すること" do
      expect{ builder.route(:index) }.to change{ route_group.size }.by(1)
      expect(route_group).to include(
        be_instance_of(RouteLink::Decoratable::Route).and have_attributes(
          name: :index,
          method_params: have_attributes(types: RouteLink::Decoratable::MethodBuilder::METHOD_TYPES),
        )
      )
    end

    it "は、追加したルーティングメソッドオブジェクトを返すこと" do
      expect(builder.route(:index)).to equal(route_group.last)
    end

    context "ブロックを渡した場合" do
      it "は、追加したルーティングメソッドオブジェクトをブロック引数に渡すこと" do
        expect{|block| builder.route(:index, &block) }.to yield_with_args(
          be_instance_of(RouteLink::Decoratable::Route).and(have_attributes(name: :index)),
        )
      end
    end
  end

  describe "#wrap" do
    describe "ブロック内self" do
      subject do
        builder.wrap({controller: Person}, {i18n_params: {i: "吾輩"}}){ return self }
      end

      it "はメソッド定義オブジェクトであること" do
        is_expected.to be_a(described_class)
      end
    end

    describe "生成されたメソッド" do
      let(:decorator){ double("Decorator", helpers: view_context).extend(route_group.mod) }

      before do
        builder.wrap({controller: Person}, {i18n_params: {i: "吾輩"}}) do
          route :index, {}, {format: :i_am_an_animal, i18n_params: {animal: "猫"}}
        end
      end

      it{ expect(decorator.index_name).to eq("吾輩は猫である") }
    end
  end
end
