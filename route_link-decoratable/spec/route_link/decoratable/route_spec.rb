require 'rails_helper'

describe RouteLink::Decoratable::Route do
  let(:route){ described_class.new(:index) }

  describe "#mod" do
    before do
      allow(route).to receive(:method_params).and_return(method_params)
      allow(method_params).to receive(:method_name){|**i18n_params| I18n.t(default: "%{as}_%{type}", **i18n_params) }
    end

    let(:method_params){ double("MethodParams", types: RouteLink::Decoratable::MethodBuilder::METHOD_TYPES) }

    context "`#method.types`が一部の種類のメソッドタイプを返す場合" do
      before do
        allow(route.method_params).to receive(:types).and_return([:path, :link])
      end

      it "は、渡した種類のメソッドを持つモジュールを返すこと" do
        expect(route.mod).to be_instance_of(Module).and have_attributes(
          instance_methods: contain_exactly(:index_path, :index_link),
        )
      end
    end

    context "`#method.types`が全ての種類のメソッドタイプを返す場合" do
      it "は、全て種類のメソッドを持つモジュールを返すこと" do
        expect(route.mod).to be_instance_of(Module).and have_attributes(
          instance_methods: contain_exactly(:index_name, :index_path, :index_url, :index_link),
        )
      end
    end

    describe "生成したメソッド" do
      let(:owner){ double("Decoratable", helpers: view_context).extend(route.mod) }

      before do
        allow(route).to receive(:route_params).and_return(route_params)
        allow(route).to receive(:link_params).and_return(link_params)
      end

      let(:route_params){ {controller: Person, action: :index, query: {a: "A1", b: "B1"}} }
      let(:link_params){ {i18n_params: {i: "吾輩", animal: "猫"}, html_attributes: {id: "id-1", class: "class-1"}} }

      describe "#index_name" do
        it{ expect(owner.index_name(:i_am_an_animal, {animal: "Cat"})).to eq("吾輩はCatである") }
      end

      describe "#index_path" do
        it{ expect(owner.index_path({b: "B2", c: "C2"})).to eq("/people?a=A1&b=B2&c=C2") }
      end

      describe "#index_url" do
        it{ expect(owner.index_url({b: "B2", c: "C2"})).to eq("http://www.example.com/people?a=A1&b=B2&c=C2") }
      end

      describe "#index_link" do
        subject do
          owner.index_link(:i_am_an_animal, {b: "B2", c: "C2"}, id: "id-2", class: "class-2", i18n_params: {animal: "Cat"})
        end

        it{ is_expected.to have_tag("a#id-2.class-1.class-2", with: {href: "/people?a=A1&b=B2&c=C2"}, text: "吾輩はCatである") }
      end

      context "にブロックを渡した場合" do
        it "は、ブロック引数にリンクビルダーを渡すこと" do
          expect{|block| owner.index_path(&block) }.to yield_with_args(be_a(RouteLink::LinkBuilder))
        end

        it "は、ブロックの戻り値を返すこと" do
          obj = double("Object")
          expect(owner.index_path{ obj }).to equal(obj)
        end
      end
    end
  end
end
