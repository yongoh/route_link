require 'rails_helper'

describe RouteLink::Decoratable::MethodCollection do
  let(:collection){ described_class.new(config) }
  let(:config){ double("Config") }
  let(:owner){ double("Owner") }

  describe "#merge!" do
    subject{ collection.merge!(other) }
    let(:other){ double("Collection", data: {}, mod: Module.new) }

    before do
      collection[:aaa] = "A1"
      collection[:bbb] = "B1"
      collection.mod.class_eval do
        def xxx; "X1" end
        def yyy; "Y1" end
      end

      other.data[:bbb] = "B2"
      other.data[:ccc] = "C2"
      other.mod.class_eval do
        def yyy; "Y2" end
        def zzz; "Z2" end
      end

      allow(other).to receive(:each){|&block| other.data.each(&block) }
    end

    it "は、自身を返すこと" do
      is_expected.to equal(collection).and have_attributes(
        config: equal(config),
      )
    end

    it "は、渡したコレクションのアイテムを自身に追加すること" do
      subject
      expect(collection.to_h).to match(aaa: "A1", bbb: "B2", ccc: "C2")
    end

    it "は、渡したコレクションのメソッドを自身のモジュールに追加すること" do
      subject
      expect(collection.mod.instance_methods).to contain_exactly(:xxx, :yyy, :zzz)
      owner.extend(collection.mod)
      expect(owner).to have_attributes(xxx: "X1", yyy: "Y2", zzz: "Z2")
    end

    it "は、渡したコレクションを変化させないこと" do
      subject
      expect(other.data).to match(bbb: "B2", ccc: "C2")
      expect(other.mod.instance_methods).to contain_exactly(:yyy, :zzz)
      owner.extend(other.mod)
      expect(owner).to have_attributes(yyy: "Y2", zzz: "Z2")
    end
  end
end
