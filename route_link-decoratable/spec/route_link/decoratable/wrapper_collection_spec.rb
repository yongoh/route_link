require 'rails_helper'

describe RouteLink::Decoratable::WrapperCollection do
  let(:collection){ described_class.new(config) }
  let(:config){ RouteLink::Config.new }

  describe "#define" do
    let(:builder){ RouteLink::Decoratable::MethodBuilder.new(route_group).extend(collection.mod) }
    let(:route_group){ RouteLink::Decoratable::RouteGroup.new(config) }

    it "は、渡した名前のインスタンスメソッドを`@mod`に定義すること" do
      collection.define(:foo){}
      expect(collection.mod.instance_methods).to contain_exactly(:foo)
    end

    describe "生成されたメソッド" do
      before do
        collection.define(:foo, controller: Person, query: {a: "A1", b: "B1", c: "C1"}) do |config_or_route|
          config_or_route.route_params(query: {b: "B2", c: "C2", d: "D2"})
        end
      end

      context "ブロック無しでパラメータを渡した場合" do
        subject do
          builder.foo(:bar, action: :baz, query: {c: "C3", d: "D3", e: "E3"})
        end

        it "は、途中までマージしたパラメータを持つルーティングオブジェクトを`#define`のブロック引数に渡すこと" do
          expect{|block|
            collection.define(:foo, &block)
            subject
          }.to yield_with_args(
            be_a(RouteLink::Decoratable::Route).and(have_attributes(
              name: :bar,
              route_params: be_instance_of(RouteLink::Resourceful::RouteParams).and(have_attributes(
                controller: Person,
                action: be_nil,
                query: match(a: "A1", b: "B2", c: "C2", d: "D2"),
              )),
            )),
          )
        end

        it "は、マージしたパラメータを持つルーティングオブジェクトを生成しグループに追加すること" do
          subject
          expect(route_group.last).to be_a(RouteLink::Decoratable::Route).and have_attributes(
            name: :bar,
            route_params: be_instance_of(RouteLink::Resourceful::RouteParams).and(have_attributes(
              controller: Person,
              action: :baz,
              query: match(a: "A1", b: "B2", c: "C3", d: "D3", e: "E3"),
            )),
          )
        end

        it "は、追加したルーティングオブジェクトを返すこと" do
          is_expected.to equal(route_group.last)
        end
      end

      context "ブロック内でルーティング生成メソッドを使った場合" do
        subject do
          builder.foo(query: {c: "C3", d: "D3", e: "E3"}) do
            path :bar, action: :baz, query: {d: "D4", e: "E4", f: "F4"}
          end
        end

        it "は、途中までマージしたパラメータを持つ設定オブジェクトを`#define`のブロック引数に渡すこと" do
          expect{|block|
            collection.define(:foo, &block)
            subject
          }.to yield_with_args(
            be_a(RouteLink::Config).and(have_attributes(
              route_params: be_instance_of(RouteLink::Resourceful::RouteParams).and(have_attributes(
                controller: Person,
                action: be_nil,
                query: match(a: "A1", b: "B2", c: "C2", d: "D2"),
              )),
            )),
          )
        end

        it "は、途中までマージしたパラメータを持つルーティングオブジェクトを生成されたメソッドのブロック引数に渡すこと" do
          expect{|block|
            builder.foo(query: {c: "C3", d: "D3", e: "E3"}, &block)
          }.to yield_with_args(
            be_a(RouteLink::Config).and(have_attributes(
              route_params: be_instance_of(RouteLink::Resourceful::RouteParams).and(have_attributes(
                controller: Person,
                action: be_nil,
                query: match(a: "A1", b: "B2", c: "C3", d: "D3", e: "E3"),
              )),
            )),
          )
        end

        it "は、マージしたパラメータを持つルーティングオブジェクトを生成しグループに追加すること" do
          subject
          expect(route_group.last).to be_a(RouteLink::Decoratable::Route).and have_attributes(
            name: :bar,
            route_params: be_instance_of(RouteLink::Resourceful::RouteParams).and(have_attributes(
              controller: Person,
              action: :baz,
              query: match(a: "A1", b: "B2", c: "C3", d: "D4", e: "E4", f: "F4"),
            )),
          )
        end

        it "は、ルーティンググループを返すこと" do
          is_expected.to equal(route_group)
        end
      end
    end
  end
end
