require 'rails_helper'

describe RouteLink::Decoratable::MethodParams do
  let(:params){ described_class.new() }

  describe "#method_name" do
    let(:params){ described_class.new.merge!(format: "%{foo}_%{bar}_%{baz}", i18n_params: {foo: "Foo", bar: "Bar"}) }

    it "は、式展開した文字列を返すこと" do
      expect(params.method_name(bar: "BAR", baz: "Baz")).to eq("Foo_BAR_Baz")
    end
  end
end
