require 'rails_helper'

describe RouteLink::Config do
  let(:config){ described_class.new }

  describe "#dup" do
    subject{ config.dup }

    it "は、新しいラッパーメソッド定義オブジェクトを持つ設定オブジェクトを返すこと" do
      expect(subject.wrappers).not_to equal(config.wrappers)
      expect(subject.wrappers).to be_instance_of(RouteLink::Decoratable::WrapperCollection).and have_attributes(
        config: equal(subject),
      )
    end

    it "は、新しいルーティンググループメソッド定義オブジェクトを持つ設定オブジェクトを返すこと" do
      expect(subject.route_groups).not_to equal(config.route_groups)
      expect(subject.route_groups).to be_instance_of(RouteLink::Decoratable::RouteGroupCollection).and have_attributes(
        config: equal(subject),
      )
    end
  end
end
