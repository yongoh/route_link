require 'route_link/decoratable/railtie'

require 'route_link/decoratable/method_params'
require 'route_link/decoratable/route'
require 'route_link/decoratable/route_group'
require 'route_link/decoratable/config'
require 'route_link/decoratable/method_collection'
require 'route_link/decoratable/wrapper_collection'
require 'route_link/decoratable/route_group_collection'
require 'route_link/decoratable/method_builder'

module RouteLink
  module Decoratable

    private

    # @return [Module] ルーティングメソッド群が定義されたモジュール（自身にインクルード済み）
    # @see RouteLink::Decoratable::Config#routes
    def routes(*args, &block)
      @routes_config ||=
        if anc = ancestors.find{|anc| anc != self && anc.respond_to?(__method__, true) }
          anc.send(__method__){|c| break c }.dup
        else
          Rails.configuration.x.route_link.dup
        end
      include(@routes_config.routes(*args, &block).mod)
    end
  end
end
