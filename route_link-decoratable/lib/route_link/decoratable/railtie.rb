module RouteLink
  module Decoratable
    class Railtie < ::Rails::Railtie

      initializer "extend Config" do
        RouteLink::Config.include(Config)
      end

      initializer "route_link-decoratable: configure route_link", after: "route_link: configure route_link" do
        Rails.configuration.x.route_link.configure do |config|
          config.method_params(
            format: "%{as}_%{type}",
            i18n_params: {},
            types: MethodBuilder::METHOD_TYPES,
          )
        end
      end
    end
  end
end
