module RouteLink
  module Decoratable
    module Config
      extend ActiveSupport::Concern

      included do

        param method_params: MethodParams

        # @!attribute [rw] wrappers
        #   @return [RouteLink::WrapperCollection] ラッパーメソッド定義オブジェクト
        # @!attribute [rw] route_group
        #   @return [RouteLink::RouteGroupCollection] ルーティンググループメソッド定義オブジェクト
        attr_writer :wrappers, :route_groups
      end

      def initialize_copy(obj)
        super(obj)
        self.wrappers = nil
        self.wrappers.merge!(obj.wrappers)
        self.route_groups = nil
        self.route_groups.merge!(obj.route_groups)
      end

      def wrappers
        @wrappers ||= RouteLink::Decoratable::WrapperCollection.new(self)
      end

      # ラッパーメソッド定義メソッド
      # @see RouteLink::Decoratable::WrapperCollection#define
      def wrap(*args, &block)
        wrappers.define(*args, &block)
      end

      def route_groups
        @route_groups ||= RouteLink::Decoratable::RouteGroupCollection.new(self)
      end

      # ルーティンググループ定義メソッド
      # @see RouteLink::Decoratable::RouteGroupCollection#define
      def route_group(*args, &block)
        route_groups.define(*args, &block)
      end

      # @yield [config] ルーティングメソッドを定義するブロック
      # @yieldparam [RouteLink::Config] config 自身
      # @yieldself [RouteLink::Decoratable::MethodBuilder] メソッド定義オブジェクト
      # @return [RouteLink::Decoratable::MethodBuilder] メソッド定義オブジェクト（上記と同じ）
      def method_builder_build(*args, &block)
        builder = method_builder_class.new(*args)
        builder.instance_exec(self, &block) if block_given?
        builder
      end

      # @return [Class<RouteLink::Decoratable::MethodBuilder>] ラッパーメソッドとルーティンググループメソッドを持つメソッドビルダークラス
      def method_builder_class
        this = self
        Class.new(Decoratable::MethodBuilder) do
          include this.wrappers.mod
          include this.route_groups.mod
        end
      end

      # @param [Array] args ルーティンググループ群の抽出メソッドに渡す引数群
      #   @see RouteLink::Decoratable::RouteGroupCollection#_extract
      # @return [RouteLink::Decoratable::RouteGroup] メソッド追加済みのルーティンググループ
      # @see #method_builder_build
      def routes(*args, &block)
        route_group = RouteGroup.new(self)
        route_group.concat(route_groups._extract(*args))
        route_group.routes(&block) if block_given?
        route_group
      end
    end
  end
end
