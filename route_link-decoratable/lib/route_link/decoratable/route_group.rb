module RouteLink
  module Decoratable
    class RouteGroup
      include Enumerable

      # @!attribute [r] config
      #   @return [RouteLink::Config] 設定オブジェクト
      attr_reader :config
      delegate *(Array.instance_methods - Object.instance_methods), to: "@collection.values".to_sym
      delegate :[], :delete, to: :@collection

      def initialize(config)
        @config = config
        @collection = {}
      end

      # ルーティングを追加
      # @param [RouteLink::Decoratable::Route] route ルーティング
      # @return [RouteLink::Decoratable::RouteGroup] 自身
      def <<(route)
        @collection[route.name] = route
        self
      end

      # ルーティング群を破壊的に連結
      # @param [RouteLink::Decoratable::RouteGroup,Array<RouteLink::Decoratable::Route>] other ルーティング群
      # @return [RouteLink::Decoratable::RouteGroup] 自身
      def concat(other)
        other.each{|route| self << route }
        self
      end

      # @return [Module] ルーティングメソッド群が定義されたモジュール
      def mod
        this = self
        Module.new do
          this.each do |route|
            include route.mod
          end
        end
      end

      # @return [RouteLink::Decoratable::RouteGroup] 抽出されたルーティング群を持つルーティンググループ
      def extract(*args, &block)
        self.class.new(config.dup).concat(_extract(*args, &block))
      end

      # ルーティング名でメソッドを絞り込む
      # @param [Array<Symbol>] only 抽出するルーティング名の配列
      # @param [Array<Symbol>] except 除外するルーティング名の配列
      # @return [Array<RouteLink::Decoratable::Route>] 抽出したルーティング群の配列
      def _extract(only: map(&:name).uniq, except: [])
        only = only.map(&:to_sym) - except.map(&:to_sym)
        select{|route| only.include?(route.name) }
      end

      # @see RouteLink::Decoratable::Config#method_builder_build
      # @see RouteLink::Decoratable::Config#merge
      def routes(*args, &block)
        if args.empty?
          config.method_builder_build(self, &block)
        else
          concat(self.class.new(config.merge(*args)).routes(&block))
        end
        self
      end
    end
  end
end
