module RouteLink
  module Decoratable
    class MethodCollection

      # @!attribute [r] config
      #   @return [RouteLink] 設定オブジェクト
      attr_reader :config
      delegate *(Hash.instance_methods - Object.instance_methods), to: :data

      def initialize(config)
        @config = config
      end

      def mod
        @mod ||= Module.new
      end

      # 渡したコレクションのメンバーアイテムとメソッドを自身にマージ
      # @param [RouteLink::Decoratable::MethodCollection] other 自身と同じ種類のコレクション
      # @return [RouteLink::Decoratable::MethodCollection] マージ済みの自身
      def merge!(other)
        data.merge!(other.data)
        mod.prepend(other.mod)
        self
      end

      protected

      # @return [Hash<Symbol => Object>] メソッド名と何かのペアのハッシュ
      def data
        @data ||= {}
      end
    end
  end
end
