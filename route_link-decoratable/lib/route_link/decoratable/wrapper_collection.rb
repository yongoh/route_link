module RouteLink
  module Decoratable
    class WrapperCollection < MethodCollection

      # パラメータもしくは処理をキューに格納
      # @param [Array] args 移譲先に渡すパラメータ群その1
      # @param [Symbol] name ラッパー名
      # @yield [config_or_route] パラメータ設定ブロック
      # @yieldparam [RouteLink::Config,RouteLink::Decoratable::Route] config_or_route 設定オブジェクトもしくはルーティングオブジェクト。
      #   作成したラッパーメソッドにブロックを渡すかどうかで変わる
      #   @see RouteLink::Decoratable::MethodBuilder#route
      #   @see RouteLink::Decoratable::MethodBuilder#wrap
      def define(name, *args, &block)
        wrapper = fetch(name){ self[name] = [] }
        wrapper << RouteLink::Config.new.merge!(*args) if args.any?
        wrapper << block if block
        define_wrapper_method(name)
        wrapper
      end

      def merge!(other)
        @data = (data.keys + other.data.keys).map(&:to_sym).uniq.map do |key|
          [key, (data.fetch(key, []) + other.data.fetch(key, [])).uniq]
        end.to_h
        mod.include(other.mod)
        self
      end

      private

      # `@mod`にラッパーメソッドを定義
      def define_wrapper_method(name)
        this = self
        mod.class_eval do

          # `#route`か`#wrap`に移譲するラッパーメソッド
          # @overload #{name}(route_name, *args)
          #   `#route`に移譲
          #   @param [Symbol] route_name ルーティング名
          #   @param [Array] args 移譲先に渡すパラメータ群その2
          #   @return [RouteLink::Decoratable::Route] 追加したルーティングオブジェクト
          # @overload #{name}(*args){}
          #   `#wrap`に移譲
          #   @param [Array] args 移譲先に渡すパラメータ群その2
          #   @yield [config] パラメータ設定ブロック
          #   @yieldparam [RouteLink::Config] config 設定オブジェクト
          #   @return [RouteLink::Decoratable::MethodBuilder] ルーティングオブジェクトの追加に使用したメソッドビルダー
          define_method(name) do |*args, &block|
            wrapper = this.fetch(name)
            if block
              wrap({}) do |config|
                wrapper.each{|c| c.is_a?(Proc) ? c.call(config) : config.merge!(c) }
                config.merge!(*args) if args.any?
                instance_exec(config, &block)
              end
            else
              route(args.shift) do |route|
                wrapper.each{|c| c.is_a?(Proc) ? c.call(route) : route.merge!(c) }
                route.merge!(*args) if args.any?
              end
            end
          end
        end
      end
    end
  end
end
