module RouteLink
  module Decoratable
    class MethodParams < Params

      # @!attribute [rw] format
      #   @return [String] メソッド名のフォーマット文字列
      # @!attribute [rw] types
      #   @return [Array<Symbol>] メソッドの種類の配列
      # @!attribute [rw] i18n_params
      #   @return [Hash] メソッド名のフォーマットパラメータ
      param :format, :types
      param :i18n_params, merge_method: :merge

      # @return [String] ルーティングメソッド名
      def method_name(**i18n_params)
        I18n.t(default: format, **self.i18n_params, **i18n_params)
      end
    end
  end
end
