module RouteLink
  module Decoratable
    class MethodBuilder

      # @return [Array] ルーティングメソッドの種類の配列
      METHOD_TYPES = %i(name path url link).freeze

      # @param [RouteLink::Decoratable::RouteGroup] route_group ルーティンググループ
      def initialize(route_group)
        @route_group = route_group
      end

      (METHOD_TYPES + [:route]).each do |method_type|

        # ルーティンググループにルーティングメソッドを追加
        define_method(method_type) do |route_name, *args, &block|
          route = Route.new(route_name).merge!(@route_group.config)
          route.merge!(*args) if args.any?
          route.method_params.types = [method_type] unless method_type === :route
          @route_group << route
          block.call(route) if block
          route
        end
      end

      # 一部パラメータを事前に渡すことができるルーティング生成ブロックメソッド
      # @yield [config] ルーティング生成用ブロック
      # @see RouteLink::Decoratable::Config#merge
      def wrap(*args, &block)
        @route_group.routes(*args, &block)
      end
    end
  end
end
