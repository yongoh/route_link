module RouteLink
  module Decoratable
    class RouteGroupCollection < MethodCollection

      # `@mod`にルーティンググループメソッドを定義
      # @param [Symbol] name ルーティンググループ名
      # @return [RouteLink::Decoratable::RouteGroup] ルーティンググループ
      def define(name, &block)
        route_group = self[name.to_sym]
        route_group.routes(&block)

        this = self
        mod.class_eval do

          # 登録されたルーティングメソッド群を一度に定義する
          # @see RouteLink::Decoratable::RouteGroup#extract
          # @return [RouteLink::Decoratable::RouteGroup] 抽出したルーティング群を持つルーティンググループ
          define_method(name) do |*args, &block|
            routes = this.fetch(name.to_sym)._extract(*args).map{|route| route.reverse_merge(@route_group.config) }
            @route_group.concat(routes)
            routes
          end
        end

        route_group
      end

      # @return [RouteLink::Decoratable::RouteGroup] 抽出されたルーティング群を持つルーティンググループ
      def extract(*args, &block)
        RouteGroup.new(config.dup).concat(_extract(*args, &block))
      end

      # @param [Hash<Symbol => Hash>] name_with_options 抽出するルーティンググループ名とその`#extract`に渡すオプションのペアのハッシュ
      #   @see RouteLink::Decoratable::RouteGroup#_extract
      # @return [Array<RouteLink::Decoratable::Route>] 抽出したルーティング群の配列
      def _extract(name_with_options = {})
        name_with_options.reduce([]) do |routes, args|
          name, options = *name_with_option_for_extraction(*args)
          routes + fetch(name)._extract(options).map{|route| route.reverse_merge(config) }
        end
      end

      protected

      # @return [Hash<Symbol => RouteLink::Decoratable::RouteGroup>] ルーティンググループ名とルーティンググループのペアのハッシュ
      def data
        @data ||= Hash.new do |hash, name|
          config = RouteLink::Config.new
          config.route_groups = self.config.route_groups.dup
          config.wrappers = self.config.wrappers.dup
          hash[name.to_sym] = RouteGroup.new(config)
        end
      end

      private

      def name_with_option_for_extraction(name, options)
        case options
        when true then options = {}
        when false then options = {only: []}
        end
        [name.to_sym, options]
      end
    end
  end
end
