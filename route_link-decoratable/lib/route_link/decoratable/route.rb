module RouteLink
  module Decoratable
    class Route
      include ParamsSet

      param route_params: RouteParams, link_params: LinkParams, method_params: MethodParams

      # @!attribute [r] name
      #   @return [Symbol] ルーティング名
      attr_reader :name

      def initialize(name)
        @name = name.to_sym
      end

      def reverse_merge(other)
        self.class.new(name).merge!(other.merge(self))
      end

      # @param [ActionView::Base] helpers ビューコンテキスト
      # @return [RouteLink::LinkBuilder] リンクビルダー
      def link_builder_build(helpers, *args, &block)
        LinkBuilder.new(helpers).merge!(self).materialize(*args, &block)
      end

      # @return [Module] 指定した種類のルーティングメソッドを持つモジュール
      def mod
        this = self
        Module.new do
          this.method_params.types.each do |method_type|
            method_name = this.method_params.method_name(as: this.name, type: method_type)

            # ルーティングメソッド
            # @yield [link_builder]
            # @yieldparam [RouteLink::LinkBuilder] リンクビルダー
            # @see RouteLink::LinkBuilder##{method_type}
            define_method(method_name) do |*args, &block|
              link_builder = this.link_builder_build(helpers, self)
              if block
                block.call(link_builder)
              else
                link_builder.public_send(method_type, *args)
              end
            end
          end
        end
      end
    end
  end
end
